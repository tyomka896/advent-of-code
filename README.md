# Advent of Code

Solutions for the [Advent of Code 2023](https://adventofcode.com/2023) using
Rust with custom [AoC-Tool](https://gitlab.com/tyomka896/advent-of-code-cli).
