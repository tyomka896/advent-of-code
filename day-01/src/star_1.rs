pub fn puzzle(input: String) -> usize {
    input.lines().fold(0, |mut acc, elem| {
        let numbers: Vec<char> = elem
            .chars()
            .filter(|elem| elem.is_numeric())
            .collect::<Vec<char>>();

        acc += format!(
            "{}{}",
            numbers.first().unwrap_or(&' '),
            numbers.last().unwrap_or(&' ')
        )
        .trim()
        .parse::<usize>()
        .unwrap_or(0);

        acc
    })
}
