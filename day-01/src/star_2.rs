use std::collections::HashMap;

pub fn puzzle(input: String) -> usize {
    let numbers_names: HashMap<&str, &str> = HashMap::from([
        ("one", "1"),
        ("two", "2"),
        ("three", "3"),
        ("four", "4"),
        ("five", "5"),
        ("six", "6"),
        ("seven", "7"),
        ("eight", "8"),
        ("nine", "9"),
    ]);

    input.lines().fold(0, |mut acc, elem| {
        let mut numbers: Vec<(usize, &str)> =
            numbers_names.iter().fold(vec![], |mut acc, (key, value)| {
                acc.append(
                    &mut elem
                        .match_indices(key)
                        .chain(elem.match_indices(value))
                        .map(|(pos, _)| (pos, *value))
                        .collect(),
                );

                acc
            });

        numbers.sort_by(|a, b| a.0.cmp(&b.0));

        acc += format!(
            "{}{}",
            numbers.first().unwrap_or(&(0, "0")).1,
            numbers.last().unwrap_or(&(0, "0")).1
        )
        .trim()
        .parse::<usize>()
        .unwrap_or(0);

        acc
    })
}
