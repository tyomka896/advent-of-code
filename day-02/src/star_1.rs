pub fn puzzle(input: String) -> usize {
    input.lines().fold(0, |mut acc, elem| {
        let game_stats: Vec<&str> = elem.splitn(2, ": ").collect();

        let game_id: usize = game_stats[0]
            .replace("Game ", "")
            .parse::<usize>()
            .unwrap_or(0);

        for stats_row in game_stats[1].split("; ") {
            let impossible: bool =
                stats_row
                    .split(", ")
                    .collect::<Vec<&str>>()
                    .iter()
                    .any(|elem| {
                        let info: Vec<&str> = elem.split(" ").collect();

                        let count: usize = info[0].parse::<usize>().unwrap_or(0);

                        match info[1] {
                            "red" if count > 12 => true,
                            "green" if count > 13 => true,
                            "blue" if count > 14 => true,
                            _ => false,
                        }
                    });

            if impossible {
                return acc;
            }
        }

        acc += game_id;

        acc
    })
}
