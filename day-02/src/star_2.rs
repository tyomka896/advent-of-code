pub fn puzzle(input: String) -> usize {
    input.lines().fold(0, |mut acc, elem| {
        let game_stats: Vec<&str> = elem.splitn(2, ": ").collect();

        let mut maxed_count: (usize, usize, usize) = (0, 0, 0);

        for stats_row in game_stats[1].split("; ") {
            stats_row
                .split(", ")
                .collect::<Vec<&str>>()
                .iter()
                .for_each(|elem| {
                    let info: Vec<&str> = elem.split(" ").collect();

                    let count: usize = info[0].parse::<usize>().unwrap_or(0);

                    match info[1] {
                        "red" => maxed_count.0 = maxed_count.0.max(count),
                        "green" => maxed_count.1 = maxed_count.1.max(count),
                        "blue" => maxed_count.2 = maxed_count.2.max(count),
                        _ => (),
                    }
                });
        }

        acc += maxed_count.0 * maxed_count.1 * maxed_count.2;

        acc
    })
}
