fn has_symbol(text: &Vec<&str>, x: i32, y: i32) -> bool {
    if x < 0 || x as usize > text.len() - 1 || y < 0 || y as usize > text[0].len() - 1 {
        return false;
    }

    let symbol: char = text[y as usize].chars().nth(x as usize).unwrap_or(' ');

    symbol != '.' && symbol.is_ascii_punctuation()
}

fn has_symbol_around(text: &Vec<&str>, x: usize, y: usize) -> bool {
    let ix: i32 = x as i32;
    let iy: i32 = y as i32;

    has_symbol(text, ix + 1, iy + 1)
        || has_symbol(text, ix, iy + 1)
        || has_symbol(text, ix - 1, iy + 1)
        || has_symbol(text, ix + 1, iy - 1)
        || has_symbol(text, ix, iy - 1)
        || has_symbol(text, ix - 1, iy - 1)
        || has_symbol(text, ix + 1, iy)
        || has_symbol(text, ix - 1, iy)
}

#[derive(Debug)]
struct Number {
    value: String,
    has_symbol: bool,
}

impl Number {
    fn new() -> Self {
        Self {
            value: String::with_capacity(3),
            has_symbol: false,
        }
    }

    fn reset(&mut self) -> &mut Self {
        *self = Self::new();

        self
    }

    fn push_number(&mut self, value: char) -> &mut Self {
        if value.is_ascii_digit() {
            self.value.push(value);
        }

        self
    }

    fn symbol_found(&mut self) -> &mut Self {
        self.has_symbol = true;

        self
    }

    fn is_empty(&self) -> bool {
        self.value.len() == 0
    }

    fn with_symbol(&self) -> bool {
        self.has_symbol
    }

    fn to_usize(&self) -> usize {
        self.value.parse::<usize>().unwrap_or(0)
    }
}

pub fn puzzle(input: String) -> usize {
    let lines: Vec<&str> = input.lines().collect::<Vec<&str>>();

    let mut number: Number = Number::new();
    let mut founded_numbers: Vec<usize> = vec![];

    for (r, row) in lines.iter().enumerate() {
        for (l, letter) in row.chars().enumerate() {
            if letter.is_ascii_punctuation() {
                if !number.is_empty() && number.with_symbol() {
                    founded_numbers.push(number.to_usize());
                }

                number.reset();

                continue;
            }

            number.push_number(letter);

            if number.with_symbol() {
                continue;
            }

            if has_symbol_around(&lines, l, r) {
                number.symbol_found();
            }
        }

        if !number.is_empty() && number.with_symbol() {
            founded_numbers.push(number.to_usize());
        }

        number.reset();
    }

    founded_numbers.iter().sum()
}
