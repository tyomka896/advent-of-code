use std::collections::HashMap;

fn has_star(text: &Vec<&str>, x: i32, y: i32) -> Option<(usize, usize)> {
    if x < 0 || x as usize > text.len() - 1 || y < 0 || y as usize > text[0].len() - 1 {
        return None;
    }

    if text[y as usize].chars().nth(x as usize).unwrap_or(' ') == '*' {
        Some((x as usize, y as usize))
    } else {
        None
    }
}

fn has_star_around(text: &Vec<&str>, x: usize, y: usize) -> Option<(usize, usize)> {
    let ix: i32 = x as i32;
    let iy: i32 = y as i32;

    has_star(text, ix + 1, iy + 1)
        .or(has_star(text, ix, iy + 1))
        .or(has_star(text, ix - 1, iy + 1))
        .or(has_star(text, ix + 1, iy - 1))
        .or(has_star(text, ix, iy - 1))
        .or(has_star(text, ix - 1, iy - 1))
        .or(has_star(text, ix + 1, iy))
        .or(has_star(text, ix - 1, iy))
}

#[derive(Debug)]
struct Number {
    value: String,
    has_star: bool,
    pos: (usize, usize),
}

impl Number {
    fn new() -> Self {
        Self {
            value: String::with_capacity(3),
            has_star: false,
            pos: (0, 0),
        }
    }

    fn reset(&mut self) -> &mut Self {
        *self = Self::new();

        self
    }

    fn push_number(&mut self, value: char) -> &mut Self {
        if value.is_ascii_digit() {
            self.value.push(value);
        }

        self
    }

    fn star_found(&mut self, x: usize, y: usize) -> &mut Self {
        self.has_star = true;
        self.pos = (x, y);

        self
    }

    fn is_empty(&self) -> bool {
        self.value.len() == 0
    }

    fn with_star(&self) -> bool {
        self.has_star
    }

    fn to_usize(&self) -> usize {
        self.value.parse::<usize>().unwrap_or(0)
    }

    fn pos_str(&self) -> String {
        format!("{}{}", self.pos.0, self.pos.1)
    }
}

pub fn puzzle(input: String) -> usize {
    let lines: Vec<&str> = input.lines().collect::<Vec<&str>>();

    let mut number: Number = Number::new();
    let mut founded_numbers: HashMap<String, Vec<usize>> = HashMap::new();

    for (r, row) in lines.iter().enumerate() {
        for (l, letter) in row.chars().enumerate() {
            if letter.is_ascii_punctuation() {
                if !number.is_empty() && number.with_star() {
                    founded_numbers
                        .entry(number.pos_str())
                        .and_modify(|elem| elem.push(number.to_usize()))
                        .or_insert(vec![number.to_usize()]);
                }

                number.reset();

                continue;
            }

            number.push_number(letter);

            if number.with_star() {
                continue;
            }

            if let Some((x, y)) = has_star_around(&lines, l, r) {
                number.star_found(x, y);
            }
        }

        if !number.is_empty() && number.with_star() {
            founded_numbers
                .entry(number.pos_str())
                .and_modify(|elem| elem.push(number.to_usize()))
                .or_insert(vec![number.to_usize()]);
        }

        number.reset();
    }

    founded_numbers.iter().fold(0, |mut acc, (_, list)| {
        if list.len() == 2 {
            acc += list.iter().fold(1, |mut mult_acc, elem| {
                mult_acc *= elem;

                mult_acc
            });
        }

        acc
    })
}
