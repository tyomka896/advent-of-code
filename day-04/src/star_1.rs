fn parse_numbers(input: &str) -> Vec<&str> {
    input.split(" ").fold(vec![], |mut acc, elem| {
        if !elem.is_empty() {
            acc.push(elem.trim());
        }

        acc
    })
}

fn parse_row(row: &str) -> (Vec<&str>, Vec<&str>) {
    let data: Vec<&str> = row.split(" | ").collect::<Vec<&str>>();

    (parse_numbers(data[0]), parse_numbers(data[1]))
}

pub fn puzzle(input: String) -> usize {
    input.lines().fold(0, |total, line| {
        let row: &str = line.split(": ").collect::<Vec<&str>>()[1];

        let (left, right) = parse_row(row);

        let sum: usize = left.iter().fold(0, |acc, elem| {
            if !right.contains(elem) {
                return acc;
            }

            match acc {
                0 => 1,
                _ => acc * 2,
            }
        });

        total + sum
    })
}
