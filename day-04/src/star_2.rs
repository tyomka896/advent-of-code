use std::collections::HashMap;

fn parse_numbers(input: &str) -> Vec<&str> {
    input.split(" ").fold(vec![], |mut acc, elem| {
        if !elem.is_empty() {
            acc.push(elem.trim());
        }

        acc
    })
}

fn parse_row(row: &str) -> (Vec<&str>, Vec<&str>) {
    let data: Vec<&str> = row.split(" | ").collect::<Vec<&str>>();

    (parse_numbers(data[0]), parse_numbers(data[1]))
}

pub fn puzzle(input: String) -> usize {
    let mut cards: HashMap<usize, usize> = HashMap::new();

    input.lines().enumerate().for_each(|(l, line)| {
        cards.entry(l).or_insert(1);

        let row: &str = line.split(": ").collect::<Vec<&str>>()[1];

        let (left, right) = parse_row(row);

        let matches: usize = left.iter().fold(0, |acc, elem| {
            if !right.contains(elem) {
                return acc;
            }

            acc + 1
        });

        for p in l + 1..=l + matches {
            let prev: usize = *cards.get(&l).unwrap();

            cards.entry(p).or_insert(1);
            cards.entry(p).and_modify(|v| *v += prev);
        }
    });

    cards.values().sum()
}
