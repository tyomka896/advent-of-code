fn parse_numbers(value: &str) -> Option<(i64, i64, i64)> {
    value
        .split_whitespace()
        .map(|elem| elem.trim().parse::<i64>())
        .into_iter()
        .collect::<Result<Vec<i64>, std::num::ParseIntError>>()
        .ok()
        .map(|elem| (elem[0], elem[1], elem[2]))
}

pub fn puzzle(input: String) -> usize {
    let mut lines = input.lines();

    let parts: Vec<&str> = lines.next().unwrap().split(": ").collect();
    lines.next();

    let mut seeds: Vec<i64> = parts[1]
        .split_whitespace()
        .map(|elem| elem.parse::<i64>().unwrap())
        .collect();
    let mut mapped_seeds: Vec<usize> = Vec::with_capacity(seeds.len());

    for line in lines {
        if line.is_empty() {
            mapped_seeds.clear();

            continue;
        }

        let Some((dest, source, range)) = parse_numbers(&line) else {
            continue;
        };

        for (l, seed) in seeds.iter_mut().enumerate() {
            if (source..source + range).contains(seed) && !mapped_seeds.contains(&l) {
                *seed += dest - source;
                mapped_seeds.push(l);
            }
        }
    }

    *seeds.iter().min().unwrap() as usize
}
