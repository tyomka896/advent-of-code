// Not entirely my solution, help from @HyperNeutrino
fn parse_numbers(value: &str) -> Option<(i64, i64, i64)> {
    value
        .split_whitespace()
        .map(|elem| elem.trim().parse::<i64>())
        .into_iter()
        .collect::<Result<Vec<i64>, std::num::ParseIntError>>()
        .ok()
        .map(|elem| (elem[0], elem[1], elem[2]))
}

pub fn puzzle(input: String) -> usize {
    let parts: Vec<&str> = input.split("\n\n").collect();

    let mut seeds: Vec<(i64, i64)> = parts[0].split(": ").collect::<Vec<&str>>()[1]
        .split_whitespace()
        .map(|elem| elem.parse::<i64>().unwrap())
        .collect::<Vec<i64>>()
        .chunks(2)
        .map(|elem| (elem[0], elem[0] + elem[1]))
        .collect();

    for block in parts[1..].iter() {
        let mut ranges: Vec<(i64, i64, i64)> = vec![];

        for line in block.lines() {
            if let Some(value) = parse_numbers(&line) {
                ranges.push(value);
            };
        }

        let mut new_seeds: Vec<(i64, i64)> = Vec::with_capacity(seeds.len());

        while let Some(seed) = seeds.pop() {
            let mut appended: bool = false;
            let (r_seed, l_seed) = seed;

            for (dest, r_start, r_end) in ranges.iter().cloned() {
                let start: i64 = r_seed.max(r_start);
                let end: i64 = l_seed.min(r_start + r_end);

                if start >= end {
                    continue;
                }

                new_seeds.push((start - r_start + dest, end - r_start + dest));

                if start > r_seed {
                    seeds.push((r_seed, start));
                }

                if l_seed > end {
                    seeds.push((end, l_seed));
                }

                appended = true;

                break;
            }

            if !appended {
                new_seeds.push((r_seed, l_seed));
            }
        }

        seeds.clone_from(&new_seeds);
    }

    seeds.sort_by(|a, b| a.0.cmp(&b.0));

    seeds[0].0 as usize
}
