fn parse_line(value: &str) -> Vec<u64> {
    let parts: Vec<&str> = value.split(": ").collect();

    parts[1].split_whitespace().fold(vec![], |mut acc, elem| {
        if !elem.is_empty() {
            acc.push(elem.parse::<u64>().unwrap());
        }

        acc
    })
}

pub fn puzzle(input: String) -> usize {
    let lines: Vec<Vec<u64>> = input
        .lines()
        .into_iter()
        .map(parse_line)
        .collect::<Vec<Vec<u64>>>();

    let times: Vec<u64> = lines[0].clone();
    let distances: Vec<u64> = lines[1].clone();

    times.iter().zip(&distances).fold(1, |total, (t, d)| {
        let ways_to_win: usize = (0..=*t).fold(0, |mut acc, l| {
            if l * (t - l) > *d {
                acc += 1;
            }

            acc
        });

        total * ways_to_win
    })
}
