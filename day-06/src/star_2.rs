fn parse_line(value: &str) -> u64 {
    let parts: Vec<&str> = value.split(": ").collect();

    parts[1].replace(" ", "").parse().unwrap()
}

pub fn puzzle(input: String) -> usize {
    let lines: Vec<u64> = input
        .lines()
        .into_iter()
        .map(parse_line)
        .collect::<Vec<u64>>();

    let time: u64 = lines[0];
    let distance: u64 = lines[1];

    let total: usize = (0..time / 2).fold(0, |acc, start| {
        if start * (time - start) <= distance {
            return acc;
        }

        acc + 1
    }) * 2;

    total + ((time + 1) % 2) as usize
}
