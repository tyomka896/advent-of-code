use std::collections::HashMap;

#[derive(Default)]
struct Card {
    hand: String,
    bid: u32,
}

impl Card {
    fn rank_of_hand(value: &str) -> char {
        let mut counter: Vec<u32> = value
            .chars()
            .fold(HashMap::new(), |mut acc, letter| {
                acc.entry(letter).and_modify(|c| *c += 1).or_insert(1);

                acc
            })
            .into_values()
            .filter(|v| *v > 1)
            .collect();

        counter.sort();

        match &counter[..] {
            &[5, ..] => 'g',
            &[4, ..] => 'f',
            &[2, 3, ..] => 'e',
            &[3, ..] => 'd',
            &[2, 2, ..] => 'c',
            &[2, ..] => 'b',
            _ => 'a',
        }
    }

    fn simplify_hand(value: &str) -> String {
        value.chars().fold(String::new(), |mut acc, card| {
            let strength: char = match card {
                val if val.is_ascii_digit() => val,
                val if val.is_alphabetic() => match val {
                    'A' => 'f',
                    'K' => 'e',
                    'Q' => 'd',
                    'J' => 'c',
                    'T' => 'b',
                    val => val,
                },
                _ => 'a',
            };

            acc.push(strength);

            acc
        })
    }
}

impl From<&str> for Card {
    fn from(value: &str) -> Self {
        let parts: Vec<&str> = value.split_whitespace().collect();

        if parts.len() != 2 {
            return Self::default();
        }

        let hand_set: String = parts[0].to_owned();
        let rank: char = Self::rank_of_hand(&hand_set);

        let hand: String = Self::simplify_hand(&format!("{}{}", rank, hand_set));

        let bid: u32 = parts[1].trim().parse::<u32>().unwrap();

        Self { hand, bid }
    }
}

pub fn puzzle(input: String) -> usize {
    let mut cards: Vec<Card> = input.lines().map(|line| line.into()).collect();

    cards.sort_by(|a, b| a.hand.cmp(&b.hand));

    cards
        .iter()
        .enumerate()
        .fold(0, |acc, (l, card)| acc + card.bid as usize * (l + 1))
}
