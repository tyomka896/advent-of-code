use std::{collections::HashMap, fmt::Debug};

#[derive(Debug, Default)]
struct Card {
    hand: String,
    rank: char,
    bid: u32,
}

impl Card {
    fn rank_of_hand(value: &str) -> char {
        let mut counter: Vec<u32> = value
            .chars()
            .fold(HashMap::new(), |mut acc, letter| {
                acc.entry(letter).and_modify(|c| *c += 1).or_insert(1);

                acc
            })
            .into_values()
            .filter(|v| *v > 1)
            .collect();

        counter.sort();

        match &counter[..] {
            &[5, ..] => 'g',
            &[4, ..] => 'f',
            &[2, 3, ..] => 'e',
            &[3, ..] => 'd',
            &[2, 2, ..] => 'c',
            &[2, ..] => 'b',
            _ => 'a',
        }
    }

    fn strongest_hand(value: &str) -> String {
        let mut hand: String = value.to_owned();

        let mut counter: Vec<(char, i32)> = value
            .chars()
            .fold(HashMap::new(), |mut acc, letter| {
                acc.entry(letter).and_modify(|c| *c += 1).or_insert(1);

                acc
            })
            .into_iter()
            .fold(vec![], |mut acc, (key, value)| {
                acc.push((key, if key == 'J' { 0 } else { value }));

                acc
            });

        counter.sort_by(|a, b| b.1.cmp(&a.1));

        if counter.contains(&('J', 0)) {
            hand = hand.replace("J", &counter[0].0.to_string());
        }

        hand
    }

    fn simplify_hand(value: &str) -> String {
        value.chars().fold(String::new(), |mut acc, card| {
            let strength: char = match card {
                val if val.is_ascii_digit() => val,
                val if val.is_alphabetic() => match val {
                    'A' => 'f',
                    'K' => 'e',
                    'Q' => 'd',
                    'T' => 'b',
                    'J' => '1',
                    val => val,
                },
                _ => 'a',
            };

            acc.push(strength);

            acc
        })
    }
}

impl From<&str> for Card {
    fn from(value: &str) -> Self {
        let parts: Vec<&str> = value.split_whitespace().collect();

        if parts.len() != 2 {
            return Self::default();
        }

        let hand_set: String = parts[0].to_owned();

        let hand: String = Self::simplify_hand(&hand_set);

        let joked_set: String = Self::strongest_hand(&hand_set);
        let rank: char = Self::rank_of_hand(&joked_set);

        let bid: u32 = parts[1].trim().parse::<u32>().unwrap();

        Self { hand, rank, bid }
    }
}

pub fn puzzle(input: String) -> usize {
    let mut cards: Vec<Card> = input.lines().map(|line| line.into()).collect();

    cards.sort_by(|a, b| a.rank.cmp(&b.rank));

    let mut sorted: Vec<&Card> = Vec::with_capacity(cards.len());

    let mut current_rank: char = cards[0].rank;
    let mut group: Vec<&Card> = vec![];

    let mut cards_iter = cards.iter();

    while let Some(card) = cards_iter.next() {
        if card.rank != current_rank && group.len() > 0 {
            group.sort_by(|a, b| a.hand.cmp(&b.hand));

            sorted.append(&mut group);

            group.clear();
        }

        current_rank = card.rank;

        group.push(card);
    }

    group.sort_by(|a, b| a.hand.cmp(&b.hand));

    sorted.append(&mut group);

    sorted
        .iter()
        .enumerate()
        .fold(0, |acc, (l, card)| acc + card.bid as usize * (l + 1))
}
