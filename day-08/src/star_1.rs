type Link = (String, String, String);

fn parse_input(input: &str) -> (Vec<bool>, Vec<(String, String, String)>) {
    let parts: Vec<&str> = input.split("\n\n").collect();

    let path: String = parts[0].to_owned();
    let data: String = parts[1].to_owned();

    let paths: Vec<bool> = path
        .chars()
        .map(|path| match path {
            'R' => true,
            'L' | _ => false,
        })
        .collect();

    let links: Vec<(String, String, String)> = data
        .lines()
        .map(|line| {
            let info: Vec<&str> = line.split(" = ").collect();

            let links: Vec<&str> = info[1][1..info[1].len() - 1].split(", ").collect();

            (info[0].to_owned(), links[0].to_owned(), links[1].to_owned())
        })
        .collect();

    (paths, links)
}

pub fn puzzle(input: String) -> usize {
    let (paths, links) = parse_input(&input);

    let mut steps: usize = 0;

    let mut link: &Link = links.iter().find(|elem| elem.0.eq("AAA")).unwrap();

    while link.0.ne("ZZZ") {
        let next = match paths[steps % paths.len()] {
            true => &link.2,
            false => &link.1,
        };

        steps += 1;

        link = links.iter().find(|elem| elem.0.eq(next)).unwrap();
    }

    steps
}
