use std::collections::HashMap;

type Link = (String, String);
type Links = HashMap<String, Link>;

fn parse_input(input: &str) -> (Vec<bool>, Links) {
    let parts: Vec<&str> = input.split("\n\n").collect();

    let path: String = parts[0].to_owned();
    let data: String = parts[1].to_owned();

    let paths: Vec<bool> = path
        .chars()
        .map(|path| match path {
            'R' => true,
            'L' | _ => false,
        })
        .collect();

    let links: Links = data.lines().fold(HashMap::new(), |mut acc, line| {
        let info: Vec<&str> = line.split(" = ").collect();

        let links: Vec<&str> = info[1][1..info[1].len() - 1].split(", ").collect();

        acc.insert(
            info[0].to_owned(),
            (links[0].to_owned(), links[1].to_owned()),
        );

        acc
    });

    (paths, links)
}

fn find_strat_points(links: &Links, letter: char) -> (Vec<&str>, Vec<&Link>) {
    let filtered = links.keys().fold(vec![], |mut acc, elem| {
        if elem.ends_with(letter) {
            acc.push(elem.as_str());
        }

        acc
    });

    let mut values = vec![];

    filtered
        .iter()
        .for_each(|elem| values.push(links.get(*elem).unwrap()));

    (filtered, values)
}

fn greatest_divisible(value: usize) -> usize {
    let val_sqrt: usize = (value as f32).sqrt() as usize;

    if val_sqrt > 2 {
        for l in 2..val_sqrt as usize {
            if value % l == 0 {
                return value / l;
            }
        }
    }

    return 1;
}

pub fn puzzle(input: String) -> usize {
    let (paths, links) = parse_input(&input);

    let mut all_steps: Vec<usize> = vec![];
    let (pairs, start_links) = find_strat_points(&links, 'A');

    for l in 0..pairs.len() {
        let mut steps: usize = 0;

        let mut pair: &str = pairs[l];
        let mut link: &Link = start_links[l];

        while !pair.ends_with('Z') {
            pair = match paths[steps % paths.len()] {
                true => &link.1,
                false => &link.0,
            };

            steps += 1;

            link = links.get(pair).unwrap();
        }

        all_steps.push(steps);
    }

    let mut total: usize = greatest_divisible(all_steps[0]);

    while let Some(value) = all_steps.pop() {
        total *= value / greatest_divisible(value);
    }

    total
}
