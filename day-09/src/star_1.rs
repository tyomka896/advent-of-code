#![allow(unused)]

fn parse_input(input: &str) -> Vec<Vec<i64>> {
    input.lines().fold(vec![], |mut acc, line| {
        acc.push(line.split(" ").map(|n| n.trim().parse().unwrap()).collect());

        acc
    })
}

pub fn puzzle(input: String) -> i64 {
    let rows: Vec<Vec<i64>> = parse_input(&input);

    let mut total_sum: i64 = 0;

    for row in rows {
        let mut sequences: Vec<Vec<i64>> = vec![row.clone()];

        let mut sequence: Vec<i64> = row.clone();

        while !sequence.iter().all(|v| v.eq(&0)) {
            let mut mapped_sequence: Vec<i64> = Vec::with_capacity(sequence.len() - 1);

            for l in 0..sequence.len() - 1 {
                mapped_sequence.push(sequence[l + 1] - sequence[l]);
            }

            sequence = mapped_sequence;

            sequences.push(sequence.clone());
        }

        let mut last_value: i64 = 0;

        for values in sequences.iter().rev().skip(1) {
            if let Some(last) = values.last() {
                last_value += *last;
            }
        }

        total_sum += last_value;
    }

    total_sum
}
