/// row, column
pub type Position = (u32, u32);
pub type Maze = Vec<Vec<Cell>>;

pub struct Cell {
    pub pipe: Pipe,
    pub visited: bool,
}

impl Cell {
    pub fn from(pipe: &Pipe) -> Self {
        Self {
            pipe: pipe.to_owned(),
            visited: false,
        }
    }
}

#[derive(Copy, Clone)]
pub enum Path {
    Up = 0,
    Right,
    Down,
    Left,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Pipe {
    UpDown,    // |
    RightLeft, // -
    TopRight,  // L
    TopLeft,   // J
    DownLeft,  // 7
    DownRight, // F
    Ground,    // .
    Start,     // S
    Inside,    // I
    Outside,   // O
}

pub fn parse_input(input: &str) -> Vec<Vec<Cell>> {
    let mut result: Vec<Vec<Cell>> = vec![];

    for line in input.lines() {
        let mut row: Vec<Cell> = vec![];

        for symbol in line.chars() {
            let cell: Cell = match symbol {
                '|' => Cell::from(&Pipe::UpDown),
                '-' => Cell::from(&Pipe::RightLeft),
                'L' => Cell::from(&Pipe::TopRight),
                'J' => Cell::from(&Pipe::TopLeft),
                '7' => Cell::from(&Pipe::DownLeft),
                'F' => Cell::from(&Pipe::DownRight),
                'S' => Cell::from(&Pipe::Start),
                '.' | _ => Cell::from(&Pipe::Ground),
            };

            row.push(cell);
        }

        result.push(row);
    }

    result
}

/// Start position in maze as S symbol
pub fn find_start_position(maze: &Maze) -> Position {
    for l in 0..maze.len() {
        for p in 0..maze[l].len() {
            if maze[l][p].pipe.eq(&Pipe::Start) {
                return (l as u32, p as u32);
            }
        }
    }

    (0, 0)
}

/// Try to find out start position pipe
pub fn find_out_start_pipe(maze: &Maze, start: &Position, around: &Vec<Position>) -> Option<Pipe> {
    let start_pipe: &Pipe = &maze[start.0 as usize][start.1 as usize].pipe;

    let mut presume: Vec<bool> = vec![false, false, false, false];

    for position in around {
        let around_pipe: &Pipe = &maze[position.0 as usize][position.1 as usize].pipe;

        if position.0 == start.0 {
            if position.1 < start.1 {
                presume[3] = able_to_go_through(start_pipe, around_pipe, &Path::Left);
            } else {
                presume[1] = able_to_go_through(start_pipe, &around_pipe, &Path::Right);
            }
        } else {
            if position.0 < start.0 {
                presume[0] = able_to_go_through(start_pipe, &around_pipe, &Path::Up);
            } else {
                presume[2] = able_to_go_through(start_pipe, &around_pipe, &Path::Down);
            }
        }
    }

    let available: &[Pipe; 8] = &[
        Pipe::UpDown,
        Pipe::RightLeft,
        Pipe::TopRight,
        Pipe::TopLeft,
        Pipe::DownLeft,
        Pipe::DownRight,
        Pipe::Ground,
        Pipe::Start,
    ];

    for pipe in available.iter() {
        if &presume[..] == directions(&pipe) {
            return Some(*pipe);
        }
    }

    None
}

/// Direction of pipes (top, right, bottom, left)
fn directions(pipe: &Pipe) -> &[bool; 4] {
    match pipe {
        Pipe::UpDown => &[true, false, true, false],
        Pipe::RightLeft => &[false, true, false, true],
        Pipe::TopRight => &[true, true, false, false],
        Pipe::TopLeft => &[true, false, false, true],
        Pipe::DownLeft => &[false, false, true, true],
        Pipe::DownRight => &[false, true, true, false],
        Pipe::Start => &[true, true, true, true],
        _ => &[false, false, false, false],
    }
}

/// Checks if able to go through from one pipe to another
pub fn able_to_go_through(from: &Pipe, to: &Pipe, path: &Path) -> bool {
    let index: usize = *path as usize;

    directions(from)[index] && directions(to)[(index + 2) % 4]
}

/// Next steps around from position in maze
pub fn find_next_steps_from(maze: &Maze, position: &Position) -> Vec<Position> {
    let mut founded: Vec<Position> = vec![];

    let row: usize = position.0 as usize;
    let col: usize = position.1 as usize;

    if col < maze[0].len() - 1
        && able_to_go_through(&maze[row][col].pipe, &maze[row][col + 1].pipe, &Path::Right)
    {
        founded.push((row as u32, col as u32 + 1));
    }
    if col > 0 && able_to_go_through(&maze[row][col].pipe, &maze[row][col - 1].pipe, &Path::Left) {
        founded.push((row as u32, col as u32 - 1));
    }
    if row < maze.len() - 1
        && able_to_go_through(&maze[row][col].pipe, &maze[row + 1][col].pipe, &Path::Down)
    {
        founded.push((row as u32 + 1, col as u32));
    }
    if row > 0 && able_to_go_through(&maze[row][col].pipe, &maze[row - 1][col].pipe, &Path::Up) {
        founded.push((row as u32 - 1, col as u32));
    }

    founded
}

/// Clear the maze from 'garbage' pipes
pub fn clear_out_maze(maze: &mut Maze) {
    for l in 0..maze.len() {
        for p in 0..maze[l].len() {
            if !maze[l][p].visited
                && maze[l][p].pipe != Pipe::Inside
                && maze[l][p].pipe != Pipe::Outside
            {
                maze[l][p].pipe = Pipe::Ground;
            }
        }
    }
}

pub fn print_maze(maze: &Maze) {
    // println!(
    //     "  {}",
    //     (0..maze[0].len())
    //         .map(|l| l.to_string())
    //         .collect::<Vec<String>>()
    //         .join(" ")
    // );

    for (_l, line) in maze.iter().enumerate() {
        let mut output: Vec<&str> = vec![];

        for p in line.iter() {
            let symbol: &str = match p.pipe {
                // Pipe::UpDown => "|",
                // Pipe::RightLeft => "-",
                // Pipe::TopRight => "L",
                // Pipe::TopLeft => "J",
                // Pipe::DownLeft => "7",
                // Pipe::DownRight => "F",
                // Pipe::Start => "S",
                Pipe::Inside => "★",
                Pipe::Outside => " ",
                Pipe::Ground => "·",
                _ => "•",
            };

            output.push(symbol);
        }

        println!("{}", output.join(" "));
    }
}
