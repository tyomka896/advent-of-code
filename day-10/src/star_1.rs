use crate::maze::*;

pub fn puzzle(input: String) -> usize {
    let mut maze: Maze = parse_input(&input);

    let start: Position = find_start_position(&maze);
    maze[start.0 as usize][start.1 as usize].visited = true;

    let mut next_steps: Vec<Position> = find_next_steps_from(&maze, &start);

    let mut total_steps: usize = 0;

    while let Some(step) = next_steps.pop() {
        total_steps += 1;

        maze[step.0 as usize][step.1 as usize].visited = true;

        next_steps = find_next_steps_from(&maze, &step)
            .iter()
            .fold(vec![], |mut acc, step| {
                if !maze[step.0 as usize][step.1 as usize].visited {
                    acc.push(*step)
                }

                acc
            });
    }

    // clear_out_maze(&mut maze);
    // print_maze(&maze);

    total_steps / 2 + total_steps % 2
}
