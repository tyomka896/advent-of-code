use crate::maze::*;

pub fn puzzle(input: String) -> usize {
    let mut maze: Maze = parse_input(&input);

    let start: Position = find_start_position(&maze);
    maze[start.0 as usize][start.1 as usize].visited = true;

    let mut next_steps: Vec<Position> = find_next_steps_from(&maze, &start);

    if let Some(value) = find_out_start_pipe(&maze, &start, &next_steps) {
        maze[start.0 as usize][start.1 as usize].pipe = value;
    };

    while let Some(step) = next_steps.pop() {
        maze[step.0 as usize][step.1 as usize].visited = true;

        next_steps = find_next_steps_from(&maze, &step)
            .iter()
            .fold(vec![], |mut acc, step| {
                if !maze[step.0 as usize][step.1 as usize].visited {
                    acc.push(*step)
                }

                acc
            });
    }

    clear_out_maze(&mut maze);

    let mut founded_inside: usize = 0;

    for l in 0..maze.len() {
        for p in 0..maze[l].len() {
            if maze[l][p].pipe.ne(&Pipe::Ground) {
                continue;
            }

            let mut intersect: i32 = 0;

            for m in p + 1..maze[l].len() {
                if [Pipe::TopRight, Pipe::TopLeft, Pipe::UpDown].contains(&maze[l][m].pipe) {
                    intersect += 1;
                }
            }

            if intersect % 2 == 0 {
                maze[l][p].pipe = Pipe::Outside;
            } else {
                founded_inside += 1;
                maze[l][p].pipe = Pipe::Inside;
            }
        }
    }

    // print_maze(&maze);

    founded_inside
}
