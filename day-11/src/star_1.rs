pub fn puzzle(input: String) -> usize {
    let lines: Vec<&str> = input.lines().collect();

    let mut empty_rows: Vec<usize> = vec![];
    let mut empty_cols: Vec<usize> = vec![];

    for (l, line) in lines.iter().enumerate() {
        if !line.contains("#") {
            empty_rows.push(l);
        }
    }

    for l in 0..lines[0].len() {
        let mut containes: bool = false;

        for p in 0..lines.len() {
            if lines[p].chars().nth(l).unwrap().eq(&'#') {
                containes = true;

                break;
            };
        }

        if !containes {
            empty_cols.push(l);
        }
    }

    let mut stars: Vec<(i64, i64)> = vec![];

    for (l, line) in lines.iter().enumerate() {
        for (p, ch) in line.chars().enumerate() {
            if !ch.eq(&'#') && !ch.is_ascii_digit() {
                continue;
            }

            let l_offset: usize = empty_rows.iter().filter(|&el| *el < l).count();
            let p_offset: usize = empty_cols.iter().filter(|&el| *el < p).count();

            stars.push(((l + l_offset) as i64, (p + p_offset) as i64));
        }
    }

    let mut distance: i64 = 0;

    for l in 0..stars.len() {
        for p in l + 1..stars.len() {
            distance += (stars[l].0 - stars[p].0).abs() + (stars[l].1 - stars[p].1).abs();
        }
    }

    distance as usize
}
