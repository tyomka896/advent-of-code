use std::collections::HashMap;

fn parse_input(input: &str) -> Vec<(String, Vec<usize>)> {
    input.lines().fold(vec![], |mut acc, line| {
        let columns: Vec<&str> = line.split_ascii_whitespace().collect();

        let numbers: Vec<usize> = columns[1]
            .split(',')
            .map(|v| v.parse::<usize>().unwrap())
            .collect();

        acc.push((vec![columns[0]; 5].join("?"), numbers.repeat(5)));

        acc
    })
}

fn format_key(record: &str, groups: &[usize]) -> String {
    format!(
        "{}|{}",
        record,
        groups.iter().map(|v| v.to_string()).collect::<String>()
    )
}

fn count_layout(record: &str, groups: &[usize], memo: &mut HashMap<String, usize>) -> usize {
    if record.len() == 0 {
        return if groups.len() == 0 { 1 } else { 0 };
    }

    if groups.len() == 0 {
        return if record.contains('#') { 0 } else { 1 };
    }

    let mut total: usize = 0;
    let first_char: char = record.chars().nth(0).unwrap();

    if first_char.ne(&'#') {
        let key: String = format_key(&record[1..], groups);

        if !memo.contains_key(&key) {
            let count: usize = count_layout(&record[1..], groups, memo);
            memo.insert(key.to_owned(), count);
        }

        total += *memo.get(&key).unwrap();
    }

    if first_char.eq(&'.') || record.len() < groups[0] || record[..groups[0]].contains('.') {
        return total;
    }

    if record.len() == groups[0] || record.chars().nth(groups[0]).unwrap().ne(&'#') {
        let record_next = if record.len() == groups[0] {
            ""
        } else {
            &record[groups[0] + 1..]
        };

        let key: String = format_key(record_next, &groups[1..]);

        if !memo.contains_key(&key) {
            let count: usize = count_layout(record_next, &groups[1..], memo);
            memo.insert(key.to_owned(), count);
        }

        total += *memo.get(&key).unwrap();
    }

    total
}

pub fn puzzle(input: String) -> usize {
    let inputs: Vec<(String, Vec<usize>)> = parse_input(&input);
    let mut total: usize = 0;

    for record in inputs {
        total += count_layout(&record.0, &record.1, &mut HashMap::<String, usize>::new());
    }

    total
}
