#[derive(Debug)]
pub struct Pattern {
    pub rows: Vec<String>,
    pub columns: Vec<String>,
}

impl Pattern {
    pub fn from(rows: &[&str]) -> Self {
        let rows: Vec<String> = rows.iter().map(|v| v.to_string()).collect();

        let mut column: Vec<char> = vec![];
        let mut columns: Vec<String> = vec![];

        for l in 0..rows[0].len() {
            for p in 0..rows.len() {
                column.push(rows[p].chars().nth(l).unwrap())
            }

            columns.push(column.iter().collect::<String>());

            column.clear();
        }

        Self { rows, columns }
    }
}

pub fn parse_input(input: &str) -> Vec<Pattern> {
    let mut patterns: Vec<Pattern> = vec![];

    let mut rows: Vec<&str> = vec![];

    for line in input.lines() {
        if line.is_empty() {
            patterns.push(Pattern::from(&rows));

            rows.clear();

            continue;
        }

        rows.push(line);
    }

    if !rows.is_empty() {
        patterns.push(Pattern::from(&rows));
    }

    patterns
}
