use crate::parse::*;

fn has_pattern_from(lines: &[&str], middle: usize) -> bool {
    let mut left: i32 = middle as i32;
    let mut right: i32 = left + 1;

    while left >= 0 && right < lines.len() as i32 {
        if lines[left as usize].ne(lines[right as usize]) {
            return false;
        }

        left -= 1;
        right += 1;
    }

    true
}

fn find_duplicates(lines: &[&str]) -> Option<usize> {
    for l in 0..lines.len() - 1 {
        if lines[l].ne(lines[l + 1]) {
            continue;
        }

        if has_pattern_from(&lines, l) {
            return Some(l + 1);
        }
    }

    None
}

pub fn puzzle(input: String) -> usize {
    let patterns: Vec<Pattern> = parse_input(&input);

    let mut total: (usize, usize) = (0, 0);

    for pattern in patterns.iter() {
        let columns: Vec<&str> = pattern.columns.iter().map(AsRef::as_ref).collect();

        if let Some(middle) = find_duplicates(&columns) {
            total.0 += middle;
        }

        let rows: Vec<&str> = pattern.rows.iter().map(AsRef::as_ref).collect();

        if let Some(middle) = find_duplicates(&rows) {
            total.1 += middle;
        }
    }

    total.0 + total.1 * 100
}
