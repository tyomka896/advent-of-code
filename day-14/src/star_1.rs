type Rocks = Vec<Vec<char>>;

fn parse(input: &str) -> Rocks {
    input.lines().fold(vec![], |mut acc, line| {
        acc.push(line.chars().collect::<Vec<char>>());

        acc
    })
}

pub fn puzzle(input: String) -> usize {
    let mut rocks: Rocks = parse(&input);

    for l in 1..rocks.len() {
        for p in 0..rocks[l].len() {
            if rocks[l][p].ne(&'O') {
                continue;
            }

            let mut move_to: usize = l;

            while move_to != 0 {
                if rocks[move_to - 1][p].eq(&'O') || rocks[move_to - 1][p].eq(&'#') {
                    break;
                }

                move_to -= 1;
            }

            rocks[l][p] = '.';
            rocks[move_to][p] = 'O';
        }
    }

    rocks.iter().enumerate().fold(0, |acc, (l, row)| {
        acc + row.iter().filter(|&v| v.eq(&'O')).count() * (rocks.len() - l)
    })
}
