type Rocks = Vec<Vec<char>>;

fn parse(input: &str) -> Rocks {
    input.lines().fold(vec![], |mut acc, line| {
        acc.push(line.chars().collect::<Vec<char>>());

        acc
    })
}

fn rocks_to_str(rocks: &Rocks) -> String {
    rocks
        .iter()
        .map(|row| row.iter().collect::<String>())
        .collect::<Vec<String>>()
        .join("\n")
}

fn cycle_rocks(rocks: &mut Rocks) {
    for tilt in 0..4 {
        for l in 0..rocks.len() {
            for p in 0..rocks[l].len() {
                let ml: usize = if tilt < 2 { l } else { rocks.len() - l - 1 };
                let mp: usize = if tilt < 3 { p } else { rocks[l].len() - p - 1 };

                if rocks[ml][mp].ne(&'O') {
                    continue;
                }

                let mut move_to: usize = if tilt % 2 == 0 { ml } else { mp };

                loop {
                    match tilt {
                        0 | 1 => {
                            if move_to == 0 {
                                break;
                            }
                        }
                        2 => {
                            if move_to >= rocks.len() - 1 {
                                break;
                            }
                        }
                        3 => {
                            if move_to >= rocks[l].len() - 1 {
                                break;
                            }
                        }
                        _ => unreachable!(),
                    }

                    let rock = match tilt {
                        0 => &rocks[move_to - 1][mp],
                        1 => &rocks[ml][move_to - 1],
                        2 => &rocks[move_to + 1][mp],
                        3 => &rocks[ml][move_to + 1],
                        _ => unreachable!(),
                    };

                    if ['O', '#'].contains(rock) {
                        break;
                    }

                    if tilt < 2 {
                        move_to -= 1
                    } else {
                        move_to += 1
                    };
                }

                rocks[ml][mp] = '.';

                if tilt % 2 == 0 {
                    rocks[move_to][mp] = 'O';
                } else {
                    rocks[ml][move_to] = 'O';
                }
            }
        }
    }
}

pub fn puzzle(input: String) -> usize {
    let mut rocks: Rocks = parse(&input);

    let mut rocks_last: String = rocks_to_str(&rocks);
    let mut rocks_history: Vec<String> = vec![rocks_last.clone()];

    for _ in 0..1_000_000 {
        cycle_rocks(&mut rocks);

        let rocks_str: String = rocks_to_str(&rocks);

        if rocks_history.contains(&rocks_str) {
            rocks_str.clone_into(&mut rocks_last);

            break;
        }

        rocks_str.clone_into(&mut rocks_last);
        rocks_history.push(rocks_str);
    }

    let cycle_start: usize = rocks_history
        .iter()
        .position(|v| v.eq(&rocks_last))
        .expect("Index must be found");

    let cycle_ends_at: usize = (1000000000 - cycle_start) % (rocks_history.len() - cycle_start);

    let rocks_ends_at: String = rocks_history[cycle_start..][cycle_ends_at].to_owned();

    rocks_ends_at.lines().enumerate().fold(0, |acc, (l, row)| {
        acc + row.chars().filter(|v| v.eq(&'O')).count() * (rocks.len() - l)
    })
}
