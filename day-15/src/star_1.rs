fn parse(input: &str) -> Vec<String> {
    input.split(",").map(|v| v.to_owned()).collect()
}

pub fn puzzle(input: String) -> usize {
    let sequences: Vec<String> = parse(&input);

    sequences.iter().fold(0, |total, seq| {
        let seq_hash: usize = seq
            .chars()
            .fold(0, |acc, ch| (acc + ch as u8 as usize) * 17 % 256);

        total + seq_hash
    })
}
