use std::collections::HashMap;

#[derive(Debug, Clone)]
struct Sequence {
    pub label: String,
    pub label_hash: usize,
    pub operation: char,
    pub focal_length: usize,
}

impl Sequence {
    pub fn from(value: &str) -> Self {
        let operation: char = if value.contains('=') { '=' } else { '-' };

        let parts: Vec<&str> = value.split(operation).collect::<Vec<&str>>();

        let label: String = parts[0].to_owned();
        let label_hash: usize = label
            .chars()
            .fold(0, |acc, ch| (acc + ch as u8 as usize) * 17 % 256);

        let focal_length = if operation.eq(&'=') {
            parts[1]
                .parse::<usize>()
                .expect("Focal length should be a number")
        } else {
            0
        };

        Self {
            label,
            label_hash,
            operation,
            focal_length,
        }
    }
}

fn parse(input: &str) -> Vec<Sequence> {
    input.split(",").map(|v| Sequence::from(&v)).collect()
}

pub fn puzzle(input: String) -> usize {
    let sequences: Vec<Sequence> = parse(&input);

    let mut boxes: HashMap<usize, Vec<Sequence>> = HashMap::new();

    for sequence in sequences.iter() {
        boxes
            .entry(sequence.label_hash)
            .and_modify(|elems| {
                if let Some(index) = elems.iter().position(|s| s.label.eq(&sequence.label)) {
                    if sequence.operation.eq(&'=') {
                        elems[index] = sequence.clone();
                    } else if sequence.operation.eq(&'-') {
                        elems.remove(index);
                    }
                } else {
                    if sequence.operation.eq(&'=') {
                        elems.push(sequence.clone());
                    }
                }
            })
            .or_insert(vec![sequence.clone()]);
    }

    boxes.iter().fold(0, |total, (key, seqs)| {
        total
            + seqs.iter().enumerate().fold(0, |acc, (l, seq)| {
                acc + (*key + 1) * (l + 1) * seq.focal_length
            })
    })
}
