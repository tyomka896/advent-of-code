#![allow(unused)]

pub type Gismo = Vec<Vec<Tile>>;

#[derive(Debug, Clone, PartialEq)]
pub enum Direction {
    Up = 0,
    Right,
    Down,
    Left,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Cell {
    Forward,
    Backward,
    Dash,
    Steep,
    Up,
    Right,
    Down,
    Left,
    Empty,
}

impl std::fmt::Display for Cell {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let symbol: char = match self {
            Cell::Forward => '/',
            Cell::Backward => '\\',
            Cell::Dash => '-',
            Cell::Steep => '|',
            Cell::Up => '^',
            Cell::Right => '>',
            Cell::Down => 'v',
            Cell::Left => '<',
            Cell::Empty => '.',
        };

        write!(f, "{}", symbol)
    }
}

impl From<Direction> for Cell {
    fn from(value: Direction) -> Self {
        match value {
            Direction::Up => Cell::Up,
            Direction::Right => Cell::Right,
            Direction::Down => Cell::Down,
            Direction::Left => Cell::Left,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Tile {
    cell: Cell,
    visits: Vec<Cell>,
}

impl Tile {
    pub fn from(value: char) -> Self {
        let cell: Cell = match value {
            '/' => Cell::Forward,
            '\\' => Cell::Backward,
            '-' => Cell::Dash,
            '|' => Cell::Steep,
            '^' => Cell::Up,
            '>' => Cell::Right,
            'v' => Cell::Down,
            '<' => Cell::Left,
            '.' | _ => Cell::Empty,
        };

        Self {
            cell,
            visits: vec![],
        }
    }

    pub fn get_cell(&self) -> Cell {
        self.cell.clone()
    }

    pub fn visit(&mut self, cell: &Cell) -> &mut Self {
        if self.cell.eq(&Cell::Empty) {
            self.cell = cell.clone();
        }

        self.visits.push(cell.clone());

        self
    }

    pub fn has_visits(&self) -> bool {
        self.visits.len() > 0
    }

    pub fn has_visited(&self, cell: &Cell) -> bool {
        self.visits.contains(cell)
    }
}

impl std::fmt::Display for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let symbol: String = match &self.cell {
            // _ => {
            //     if self.visits.len() > 0 {
            //         "#".to_string()
            //     } else {
            //         Cell::Empty.to_string()
            //     }
            // }
            Cell::Forward | Cell::Backward | Cell::Dash | Cell::Steep | Cell::Empty => {
                self.cell.to_string()
            }
            Cell::Up | Cell::Right | Cell::Down | Cell::Left => {
                if self.visits.len() > 1 {
                    self.visits.len().to_string()
                } else {
                    self.cell.to_string()
                }
            }
        };

        write!(f, "{}", symbol)
    }
}

#[derive(Debug, Clone)]
pub struct Walker {
    x: isize,
    y: isize,
    direction: Direction,
    stopped: bool,
}

impl Walker {
    pub fn from(position: (isize, isize), direction: &Direction) -> Self {
        Self {
            x: position.0,
            y: position.1,
            direction: direction.clone(),
            stopped: false,
        }
    }

    pub fn position(&self) -> (usize, usize) {
        (self.x as usize, self.y as usize)
    }

    pub fn within_gismo(&self, gismo: &Gismo) -> bool {
        if self.stopped
            || self.x < 0
            || self.x as usize >= gismo[0].len()
            || self.y < 0
            || self.y as usize >= gismo.len()
        {
            false
        } else {
            true
        }
    }

    pub fn handle_tile(&mut self, tile: &mut Tile) -> Option<Walker> {
        let entered_direction: Direction = self.direction.clone();
        let tile_cell: Cell = tile.get_cell();

        let mut splitted: Option<Walker> = None;

        match &tile_cell {
            Cell::Forward => {
                let visited: bool = match &self.direction {
                    Direction::Up | Direction::Left => {
                        tile.has_visited(&Cell::Up) || tile.has_visited(&Cell::Left)
                    }
                    Direction::Right | Direction::Down => {
                        tile.has_visited(&Cell::Right) || tile.has_visited(&Cell::Down)
                    }
                };

                if visited {
                    self.stopped = true;
                } else {
                    self.direction = match &self.direction {
                        Direction::Up => Direction::Right,
                        Direction::Right => Direction::Up,
                        Direction::Down => Direction::Left,
                        Direction::Left => Direction::Down,
                    };
                }
            }
            Cell::Backward => {
                let visited: bool = match &self.direction {
                    Direction::Up | Direction::Right => {
                        tile.has_visited(&Cell::Up) || tile.has_visited(&Cell::Right)
                    }
                    Direction::Left | Direction::Down => {
                        tile.has_visited(&Cell::Left) || tile.has_visited(&Cell::Down)
                    }
                };

                if visited {
                    self.stopped = true;
                } else {
                    self.direction = match &self.direction {
                        Direction::Up => Direction::Left,
                        Direction::Right => Direction::Down,
                        Direction::Down => Direction::Right,
                        Direction::Left => Direction::Up,
                    };
                }
            }
            Cell::Dash => {
                let visited: bool = match &self.direction {
                    Direction::Up | Direction::Down => {
                        tile.has_visited(&Cell::Up) || tile.has_visited(&Cell::Down)
                    }
                    _ => false,
                };

                if visited {
                    self.stopped = true;
                } else if [Direction::Up, Direction::Down].contains(&self.direction) {
                    let mut splitted_walker = Walker::from((self.x, self.y), &Direction::Left);
                    splitted_walker.next_step();

                    splitted = Some(splitted_walker);

                    self.direction = Direction::Right;
                }
            }
            Cell::Steep => {
                let visited: bool = match &self.direction {
                    Direction::Left | Direction::Right => {
                        tile.has_visited(&Cell::Left) || tile.has_visited(&Cell::Right)
                    }
                    _ => false,
                };

                if visited {
                    self.stopped = true;
                } else if [Direction::Right, Direction::Left].contains(&self.direction) {
                    let mut splitted_walker = Walker::from((self.x, self.y), &Direction::Up);
                    splitted_walker.next_step();

                    splitted = Some(splitted_walker);

                    self.direction = Direction::Down;
                }
            }
            _ => (),
        }

        self.next_step();

        tile.visit(&entered_direction.into());

        splitted
    }

    fn next_step(&mut self) {
        let (x, y) = match &self.direction {
            Direction::Up => (0, -1),
            Direction::Right => (1, 0),
            Direction::Down => (0, 1),
            Direction::Left => (-1, 0),
        };

        self.x += x;
        self.y += y;
    }
}

pub fn parse(input: &str) -> Gismo {
    input
        .lines()
        .fold(vec![], |mut acc: Vec<Vec<Tile>>, line: &str| {
            let tiles: Vec<Tile> = line.chars().map(|v: char| Tile::from(v)).collect();

            acc.push(tiles);

            acc
        })
}

pub fn print_gismo(gismo: &Gismo) {
    gismo.iter().for_each(|row| {
        println!("{}", row.iter().map(|v| v.to_string()).collect::<String>());
    });
}
