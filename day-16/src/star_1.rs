#![allow(unused)]

use super::gismo::*;

pub fn puzzle(input: String) -> usize {
    let mut gismo: Gismo = parse(&input);

    let mut walkers: Vec<Walker> = vec![Walker::from((0, 0), &Direction::Right)];

    while let Some(walker) = walkers.pop().as_mut() {
        while walker.within_gismo(&gismo) {
            let (x, y) = walker.position();

            let mut tile: &mut Tile = &mut gismo[y][x];

            if let Some(splitted) = walker.handle_tile(&mut tile) {
                walkers.push(splitted);
            }
        }
    }

    gismo.iter().fold(0, |total, row| {
        total + row.iter().filter(|&v| v.has_visits()).count()
    })
}
