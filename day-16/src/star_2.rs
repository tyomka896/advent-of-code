#![allow(unused)]

use super::gismo::*;

fn walk(gismo_init: &Gismo, walker_start: Walker) -> usize {
    let mut gismo: Gismo = gismo_init.clone();

    let mut walkers: Vec<Walker> = vec![walker_start];

    while let Some(walker) = walkers.pop().as_mut() {
        while walker.within_gismo(&gismo) {
            let (x, y) = walker.position();

            let mut tile: &mut Tile = &mut gismo[y][x];

            if let Some(splitted) = walker.handle_tile(&mut tile) {
                walkers.push(splitted);
            }
        }
    }

    gismo.iter().fold(0, |total, row| {
        total + row.iter().filter(|&v| v.has_visits()).count()
    })
}

pub fn puzzle(input: String) -> usize {
    let mut gismo: Gismo = parse(&input);

    let mut total_max: usize = 0;

    let last: isize = gismo.len() as isize - 1;

    for l in 0..gismo.len() as isize {
        total_max = total_max
            .max(walk(&gismo, Walker::from((0, l), &Direction::Right)))
            .max(walk(&gismo, Walker::from((0, last), &Direction::Left)));
    }

    let last: isize = gismo[0].len() as isize - 1;

    for l in 0..gismo[0].len() as isize {
        total_max = total_max
            .max(walk(&gismo, Walker::from((l, 0), &Direction::Down)))
            .max(walk(&gismo, Walker::from((last, 0), &Direction::Up)));
    }

    total_max
}
