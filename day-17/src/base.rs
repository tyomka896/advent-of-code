use std::collections::{BinaryHeap, HashSet};

#[derive(Debug, Clone, Eq)]
pub struct Node {
    pub w: u32,
    pub x: u32,
    pub y: u32,
    pub dx: i32,
    pub dy: i32,
    pub n: u32,
    pub history: Vec<(u32, u32)>,
}

impl Node {
    pub fn from(w: u32, x: u32, y: u32, dx: i32, dy: i32, n: u32) -> Self {
        Self {
            w,
            x,
            y,
            dx,
            dy,
            n,
            history: vec![(x, y)],
        }
    }

    pub fn step(&self, w: u32, dx: i32, dy: i32, n: u32) -> Self {
        let x: u32 = (self.x as i32 + dx) as u32;
        let y: u32 = (self.y as i32 + dy) as u32;

        let mut history: Vec<(u32, u32)> = self.history.clone();
        history.push((x, y));

        Self {
            w,
            x,
            y,
            dx,
            dy,
            n,
            history,
        }
    }
}

impl std::fmt::Display for Node {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let Node {
            w, x, y, dx, dy, n, ..
        } = self;

        write!(f, "{} ({},{}) [{},{}] {}", w, x, y, dx, dy, n)
    }
}

impl Ord for Node {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.w.cmp(&self.w)
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        match self.cmp(other) {
            std::cmp::Ordering::Equal => true,
            _ => false,
        }
    }
}

/// * `graph` - Graph represented as 2d array.
/// * `min_steps` - Minimum straight steps to make.
/// * `max_steps` - Maximum straight steps to make even before it can stop.
pub fn shortest_path(graph: &Vec<Vec<u32>>, min_steps: u32, max_steps: u32) -> usize {
    println!("(min_steps, max_steps) -> ({}, {:?})", min_steps, max_steps);

    let mut shortest: Option<Node> = None;

    let mut visits: HashSet<(u32, u32, i32, i32, u32)> = HashSet::new();

    // Don't know how to use it at the end
    // Here for complete Dijkstra's algorithm
    let mut weights: Vec<Vec<u32>> = vec![vec![]; graph.len()]
        .iter()
        .map(|_: &Vec<Vec<u32>>| vec![u32::MAX; graph[0].len()])
        .collect::<Vec<Vec<u32>>>();
    weights[0][0] = 0;

    // Tried to implement it with custom PriorityQueue
    // It was slow, so switched to BinaryHeap
    let mut queue: BinaryHeap<Node> = BinaryHeap::new();
    queue.push(Node::from(0, 0, 0, 0, 0, 0));

    while let Some(node) = queue.pop() {
        let Node {
            w, x, y, dx, dy, n, ..
        } = node;

        // The target cell, node at bottom right corner and N steps straight even before it stops
        if x as usize == graph[0].len() - 1 && y as usize == graph.len() - 1 && n >= min_steps {
            shortest = Some(node.clone());

            break;
        }

        if visits.contains(&(x, y, dx, dy, n)) {
            continue;
        }

        visits.insert((x, y, dx, dy, n));

        // Go straight while N steps not exceeded
        if n < max_steps && (dx, dy) != (0, 0) {
            let nx: i32 = x as i32 + dx;
            let ny: i32 = y as i32 + dy;

            if within_graph(&graph, nx, ny) {
                let weight: u32 = w + graph[ny as usize][nx as usize];

                weights[ny as usize][nx as usize] = weights[ny as usize][nx as usize].min(weight);

                queue.push(node.step(weight, dx, dy, n + 1));
            }
        }

        // Don't go to other directions  unless you've passed 4 steps straight
        // Except the step from upper right corner (start position)
        if n < min_steps && (dx, dy) != (0, 0) {
            continue;
        }

        // Go to other directions
        for (ndx, ndy) in [(0, -1), (1, 0), (0, 1), (-1, 0)] {
            // Don't go back or straight, must to turn left or right
            if (ndx, ndy) == (dx, dy) || (ndx, ndy) == (-dx, -dy) {
                continue;
            }

            let nx: i32 = x as i32 + ndx;
            let ny: i32 = y as i32 + ndy;

            if !within_graph(&graph, nx, ny) {
                continue;
            }

            let weight: u32 = w + graph[ny as usize][nx as usize];

            weights[ny as usize][nx as usize] = weights[ny as usize][nx as usize].min(weight);

            queue.push(node.step(weight, ndx, ndy, 1));
        }
    }

    if let Some(node) = shortest {
        // _print_graph_history(&graph, &node.history);

        node.w as usize
    } else {
        0
    }
}

pub fn within_graph(graph: &Vec<Vec<u32>>, x: i32, y: i32) -> bool {
    if x >= 0 && x < graph[0].len() as i32 && y >= 0 && y < graph.len() as i32 {
        return true;
    }

    false
}

pub fn parse(input: &str) -> Vec<Vec<u32>> {
    input.lines().fold(vec![], |mut acc, line| {
        acc.push(
            line.trim()
                .chars()
                .map(|v| v.to_string().parse::<u32>().unwrap())
                .collect(),
        );

        acc
    })
}

pub fn _print_graph_history(graph: &Vec<Vec<u32>>, history: &Vec<(u32, u32)>) {
    for (l, row) in graph.iter().enumerate() {
        let mut line: Vec<String> = vec![];

        for (p, _value) in row.iter().enumerate() {
            if history.contains(&(p as u32, l as u32)) {
                line.push("#".to_string());
            } else {
                line.push(".".to_string());
            }
        }

        println!("{}", line.join(""));
    }
}
