use super::base::*;

pub fn puzzle(input: String) -> usize {
    let graph: Vec<Vec<u32>> = parse(&input);

    shortest_path(&graph, 0, 3)
}
