pub fn parse(input: &str) -> Vec<((i32, i32), i32)> {
    input.lines().fold(vec![], |mut acc, line| {
        let parts: Vec<&str> = line.split_whitespace().collect::<Vec<&str>>();

        let direction: char = parts[0]
            .chars()
            .next()
            .expect("Direction must not be empty");

        let path: (i32, i32) = match direction {
            'U' => (0, -1),
            'R' => (1, 0),
            'D' => (0, 1),
            'L' => (-1, 0),
            _ => unreachable!(),
        };

        let steps: i32 = parts[1]
            .parse::<i32>()
            .expect("Count of steps must be a number");

        acc.push((path, steps));

        acc
    })
}

pub fn puzzle(input: String) -> usize {
    let data: Vec<((i32, i32), i32)> = parse(&input);

    let mut x: i32 = 0;
    let mut y: i32 = 0;

    let mut area: f32 = 0.0;
    let mut perimeter: i32 = 0;

    // Shoelace formula with the sum of 2x2 determinants
    for ((dx, dy), step) in data.iter() {
        perimeter += step;

        let nx: i32 = dx * step;
        let ny: i32 = dy * step;

        area += (x * ny - y * nx) as f32;

        x += nx;
        y += ny;
    }

    let area: i32 = (area.abs() / 2.0) as i32;

    // Pick's theorem
    let within: i32 = (area - (perimeter / 2) + 1) as i32;

    (perimeter + within) as usize
}
