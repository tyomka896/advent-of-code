pub fn parse(input: &str) -> Vec<((i64, i64), i64)> {
    input.lines().fold(vec![], |mut acc, line| {
        let parts: Vec<&str> = line.split_whitespace().collect::<Vec<&str>>();

        let hex_value: &str = &parts[2].replace(['(', '#', ')'], "");

        let steps: i64 = i64::from_str_radix(&hex_value[..hex_value.len() - 1], 16)
            .expect("Count of steps in hex must be a number");
        let direction: char = hex_value
            .chars()
            .last()
            .expect("Direction in hex must not be empty");

        let path: (i64, i64) = match direction {
            '0' => (0, -1),
            '1' => (1, 0),
            '2' => (0, 1),
            '3' => (-1, 0),
            _ => unreachable!(),
        };

        acc.push((path, steps));

        acc
    })
}

pub fn puzzle(input: String) -> usize {
    let data: Vec<((i64, i64), i64)> = parse(&input);

    let mut x: i64 = 0;
    let mut y: i64 = 0;

    let mut area: f64 = 0.0;
    let mut perimeter: i64 = 0;

    // Shoelace formula with the sum of 2x2 determinants
    for ((dx, dy), step) in data.iter() {
        perimeter += step;

        let nx: i64 = dx * step;
        let ny: i64 = dy * step;

        area += (x * ny - y * nx) as f64;

        x += nx;
        y += ny;
    }

    let area: i64 = (area.abs() / 2.0) as i64;

    // Pick's theorem
    let within: i64 = (area - (perimeter / 2) + 1) as i64;

    (perimeter + within) as usize
}
