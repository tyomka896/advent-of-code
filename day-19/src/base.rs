#![allow(unused)]

use std::collections::HashMap;

#[derive(Debug)]
pub enum Part {
    X,
    M,
    A,
    S,
}

impl Part {
    pub fn as_char(&self) -> char {
        match self {
            Part::X => 'x',
            Part::M => 'm',
            Part::A => 'a',
            Part::S => 's',
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum Sign {
    Less,
    Greater,
}

impl std::fmt::Display for Sign {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let value: char = match self {
            Sign::Less => '<',
            Sign::Greater => '>',
        };

        write!(f, "{}", value)
    }
}

#[derive(Debug, PartialEq)]
pub enum Status {
    Send(String),
    Accept,
    Reject,
    Empty,
}

impl std::fmt::Display for Status {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let value = match self {
            Status::Send(value) => value,
            Status::Accept => "A",
            Status::Reject => "R",
            Status::Empty => "E",
        };

        write!(f, "{}", value)
    }
}

impl Status {
    pub fn from(value: &str) -> Self {
        match value {
            "A" => Self::Accept,
            "R" => Self::Reject,
            name => Self::Send(name.to_string()),
        }
    }
}

#[derive(Debug)]
pub struct Rule {
    pub part: Part,
    pub sign: Sign,
    pub value: u32,
    pub left: Status,
    pub right: Option<Status>,
}

impl From<&str> for Rule {
    fn from(value: &str) -> Self {
        let parts: Vec<&str> = value.split(":").collect::<Vec<&str>>();

        let conditions: Vec<&str> = parts[0].split(['<', '>']).collect::<Vec<&str>>();

        let part: Part = match conditions[0].chars().next().expect("Part must be valid") {
            'x' => Part::X,
            'm' => Part::M,
            'a' => Part::A,
            's' => Part::S,
            _ => unreachable!(),
        };

        let sign: Sign = if parts[0].contains("<") {
            Sign::Less
        } else if parts[0].contains(">") {
            Sign::Greater
        } else {
            panic!("Condition must be valid")
        };

        let value: u32 = conditions[1]
            .parse::<u32>()
            .expect("Value of condition should be valid");

        let statuses: Vec<&str> = parts[1].split(',').collect::<Vec<&str>>();

        let left: Status = Status::from(statuses[0]);

        let right: Option<Status> = if statuses.len() > 1 {
            Some(Status::from(statuses[1]))
        } else {
            None
        };

        Self {
            part,
            sign,
            value,
            left,
            right,
        }
    }
}

impl std::fmt::Display for Rule {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let right = if let Some(value) = &self.right {
            format!(",{}", value)
        } else {
            "".to_string()
        };

        write!(
            f,
            "{:?}{}{}:{}{}",
            self.part, self.sign, self.value, self.left, right
        )
    }
}

#[derive(Debug)]
pub struct Workflow {
    name: String,
    rules: Vec<Rule>,
}

impl From<&str> for Workflow {
    fn from(value: &str) -> Self {
        let parts: Vec<&str> = value.split(['{', '}']).collect::<Vec<&str>>();

        let name: String = parts[0].to_string();

        let commas_len: usize = parts[1].chars().filter(|c| c.eq(&',')).count();

        let rules: Vec<Rule> = parts[1]
            .splitn(commas_len, ',')
            .map(|elem| elem.into())
            .collect::<Vec<Rule>>();

        Self { name, rules }
    }
}

impl std::fmt::Display for Workflow {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let rules: String = self
            .rules
            .iter()
            .fold(vec![], |mut acc, rule| {
                acc.push(format!("({})", rule));

                acc
            })
            .join(", ");

        write!(f, "{} -> [{}]", self.name, rules)
    }
}

#[derive(Debug)]
pub struct Rating {
    pub x: u32,
    pub m: u32,
    pub a: u32,
    pub s: u32,
    pub status: Status,
}

impl From<&str> for Rating {
    fn from(value: &str) -> Self {
        let parts: Vec<&str> = value.split(['{', '}']).collect::<Vec<&str>>();

        let parts: Vec<u32> = parts[1]
            .split(',')
            .collect::<Vec<&str>>()
            .iter()
            .map(|&elem| {
                elem.split('=').collect::<Vec<&str>>()[1]
                    .parse::<u32>()
                    .unwrap()
            })
            .collect::<Vec<u32>>();

        Self {
            x: parts[0],
            m: parts[1],
            a: parts[2],
            s: parts[3],
            status: Status::Empty,
        }
    }
}

// impl std::fmt::Display for Rating {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         write!(
//             f,
//             "x={},m={},a={},s={} ({})",
//             self.x, self.m, self.a, self.s, self.status
//         )
//     }
// }

pub fn parse(input: &str) -> (HashMap<String, Vec<Rule>>, Vec<Rating>) {
    let mut workflows: HashMap<String, Vec<Rule>> = HashMap::new();
    let mut ratings: Vec<Rating> = vec![];

    let mut lines = input.lines();

    while let Some(line) = lines.next() {
        if line.is_empty() {
            break;
        }

        let Workflow { name, rules } = line.into();

        workflows.insert(name, rules);
    }

    while let Some(line) = lines.next() {
        ratings.push(line.into());
    }

    (workflows, ratings)
}
