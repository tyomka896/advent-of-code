use std::collections::HashMap;

use super::base::*;

fn define_status(workflows: &HashMap<String, Vec<Rule>>, rating: &mut Rating) {
    let mut rules: &Vec<Rule> = workflows.get("in").expect("Rule 'in' must exists");

    loop {
        for rule in rules.iter() {
            let value: u32 = match rule.part {
                Part::X => rating.x,
                Part::M => rating.m,
                Part::A => rating.a,
                Part::S => rating.s,
            };

            let send_to: Option<&Status> = match rule.sign {
                Sign::Less if value < rule.value => Some(&rule.left),
                Sign::Greater if value > rule.value => Some(&rule.left),
                _ => rule.right.as_ref(),
            };

            let Some(send) = send_to else {
                continue;
            };

            match send {
                Status::Send(key) => {
                    rules = workflows
                        .get(key)
                        .expect(&format!("Rule '{}' must exists", key));
                }
                Status::Accept => rating.status = Status::Accept,
                Status::Reject => rating.status = Status::Reject,
                _ => (),
            }

            break;
        }

        if rating.status.eq(&Status::Accept) || rating.status.eq(&Status::Reject) {
            break;
        }
    }
}

pub fn puzzle(input: String) -> usize {
    let (workflows, mut ratings) = parse(&input);

    ratings
        .iter_mut()
        .for_each(|elem| define_status(&workflows, elem));

    ratings.iter().fold(0, |acc, rating| {
        if rating.status.eq(&Status::Reject) {
            return acc;
        }

        let Rating { x, m, a, s, .. } = rating;

        acc + x + m + a + s
    }) as usize
}
