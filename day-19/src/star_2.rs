#![allow(unused)]

use std::{collections::HashMap, io::Write, ops};

use super::base::*;

fn combinations(
    workflows: &HashMap<String, Vec<Rule>>,
    ranges: &mut HashMap<char, (u64, u64)>,
    status: &Status,
) -> u64 {
    let status_key: String = match status {
        Status::Reject | Status::Empty => return 0,
        Status::Accept => {
            let product: u64 = ranges.iter().fold(1, |acc, (_, (l, r))| acc * (r - l + 1));

            return product;
        }
        Status::Send(value) => value.to_owned(),
    };

    let mut total: u64 = 0;

    let rules: &Vec<Rule> = workflows
        .get(&status_key)
        .expect(&format!("Rule '{}' must exists", status_key));

    for rule in rules.iter() {
        let (l, r) = ranges
            .get(&rule.part.as_char())
            .expect("Range name not found");

        let (l_range, r_range) = match rule.sign {
            Sign::Less => ((*l, rule.value as u64 - 1), (rule.value as u64, *r)),
            Sign::Greater => ((rule.value as u64 + 1, *r), (*l, rule.value as u64)),
        };

        if l_range.0 <= l_range.1 {
            let mut ranges_c: HashMap<char, (u64, u64)> = ranges.clone();
            ranges_c
                .entry(rule.part.as_char())
                .and_modify(|elem| *elem = l_range);

            total += combinations(workflows, &mut ranges_c, &rule.left);
        }

        if r_range.0 <= r_range.1 {
            ranges
                .entry(rule.part.as_char())
                .and_modify(|elem| *elem = r_range);
        }

        if let Some(status) = &rule.right {
            total += combinations(workflows, ranges, status);
        }
    }

    total
}

pub fn puzzle(input: String) -> usize {
    let (workflows, _) = parse(&input);

    let mut ranges: HashMap<char, (u64, u64)> = HashMap::from([
        ('x', (1, 4000)),
        ('m', (1, 4000)),
        ('a', (1, 4000)),
        ('s', (1, 4000)),
    ]);

    // Don't fully understand DP, so this was difficult to comprehend to me at the moment 🤷‍♂️
    combinations(&workflows, &mut ranges, &Status::Send("in".to_string())) as usize
}
