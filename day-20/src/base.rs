use std::collections::HashMap;

#[derive(Debug, Clone, PartialEq)]
pub enum Pulse {
    Low,
    High,
    Empty,
}

impl std::fmt::Display for Pulse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let word = match self {
            Pulse::Low => "Low",
            Pulse::High => "High",
            Pulse::Empty => "Empty",
        };

        write!(f, "{}", word)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum State {
    On,
    Off,
}

impl std::fmt::Display for State {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let word = match self {
            State::On => "On",
            State::Off => "Off",
        };

        write!(f, "{}", word)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Module {
    Broadcaster,
    FlipFlop,
    Conjunction,
    Untyped,
}

impl Module {
    pub fn from(name: &str) -> Self {
        if name.eq("broadcaster") {
            return Module::Broadcaster;
        }

        let first: char = name.chars().next().unwrap();

        match first {
            '%' => Module::FlipFlop,
            '&' => Module::Conjunction,
            _ => Module::Untyped,
        }
    }
}

impl std::fmt::Display for Module {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let word = match self {
            Module::Broadcaster => "broadcaster",
            Module::FlipFlop => "flip-flop",
            Module::Conjunction => "conjunction",
            Module::Untyped => "untyped",
        };

        write!(f, "{}", word)
    }
}

#[derive(Debug, Clone)]
pub struct CablePulse {
    pub name: String,
    pub pulse: Pulse,
}

impl std::fmt::Display for CablePulse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "\"{}\" ({})", self.name, self.pulse)
    }
}

impl CablePulse {
    pub fn from(name: &str) -> Self {
        Self {
            name: name.to_owned(),
            pulse: Pulse::Low,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Cable {
    pub name: String,
    pub module: Module,
    pub state: State,
    pub pulse: Pulse,
    pub inputs: Vec<CablePulse>,
    pub outputs: Vec<String>,
}

impl std::fmt::Display for Cable {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let outs = self
            .inputs
            .iter()
            .map(|el| el.to_string())
            .collect::<Vec<String>>()
            .join(", ");

        write!(
            f,
            "[{}] '{}' is {}, sate is {}, has INs [{}] and OUTs {:?}",
            self.module, self.name, self.state, self.pulse, outs, self.outputs
        )
    }
}

impl Default for Cable {
    fn default() -> Self {
        Self {
            name: String::from("unknown"),
            module: Module::Untyped,
            state: State::Off,
            pulse: Pulse::Empty,
            inputs: vec![],
            outputs: vec![],
        }
    }
}

impl Cable {
    pub fn from(value: &str) -> Self {
        let name: String = if value.eq("broadcaster") {
            "broadcaster".to_owned()
        } else {
            value.chars().skip(1).into_iter().collect()
        };

        let module: Module = Module::from(value);

        let pulse: Pulse = match module {
            Module::Broadcaster | Module::Conjunction => Pulse::Low,
            _ => Pulse::Empty,
        };

        Self {
            name,
            module,
            pulse,
            ..Default::default()
        }
    }

    pub fn get_pulse(&self) -> &Pulse {
        match self.module {
            Module::Conjunction => {
                if self.inputs.iter().any(|el| el.pulse.eq(&Pulse::Low)) {
                    &Pulse::High
                } else {
                    &Pulse::Low
                }
            }
            _ => &self.pulse,
        }
    }

    pub fn set_pulse(&mut self, pulse: &Pulse, name: &str) {
        match self.module {
            Module::FlipFlop => {
                if pulse.eq(&Pulse::High) {
                    self.pulse = Pulse::Empty;

                    return;
                }

                self.state = match self.state {
                    State::On => State::Off,
                    State::Off => State::On,
                };

                self.pulse = match self.state {
                    State::On => Pulse::High,
                    State::Off => Pulse::Low,
                };
            }
            Module::Conjunction => {
                for cable_pulse in self.inputs.iter_mut() {
                    if cable_pulse.name.eq(name) {
                        cable_pulse.pulse = pulse.clone();

                        break;
                    }
                }
            }
            Module::Untyped => {
                self.pulse = pulse.clone();
            }
            _ => (),
        }
    }

    pub fn get_outputs(&self) -> Vec<String> {
        if self.module.eq(&Module::FlipFlop) && self.pulse.eq(&Pulse::Empty) {
            return vec![];
        }

        self.outputs.clone()
    }
}

pub fn parse(input: &str) -> HashMap<String, Cable> {
    input
        .lines()
        .fold(HashMap::<String, Cable>::new(), |mut acc, line| {
            let parts: Vec<&str> = line.split(" -> ").collect();

            let outputs: Vec<&str> = parts[1].split(",").map(|el| el.trim()).collect();

            let mut cable: Cable = Cable::from(parts[0]);
            cable.outputs = outputs.iter().map(|&el| el.to_owned()).collect();

            let cable_name: String = cable.name.to_owned();

            acc.entry(cable_name.to_owned())
                .and_modify(|el| {
                    el.name = cable_name.to_owned();
                    el.module = Module::from(parts[0]);
                    el.outputs = outputs.iter().map(|&el| el.to_owned()).collect();
                })
                .or_insert(cable);

            for output in outputs {
                acc.entry(output.to_string())
                    .and_modify(|el| {
                        el.inputs.push(CablePulse::from(&cable_name));
                    })
                    .or_insert_with(|| {
                        let mut cable: Cable = Cable::default();
                        cable.name = parts[1].to_owned();
                        cable.inputs = vec![CablePulse::from(&cable_name)];

                        cable
                    });
            }

            acc
        })
}
