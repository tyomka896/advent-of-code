pub fn gcd(mut a: usize, mut b: usize) -> usize {
    if a > b {
        std::mem::swap(&mut a, &mut b);
    }

    while a > 0 {
        b %= a;

        std::mem::swap(&mut a, &mut b);
    }

    b
}

pub fn lcm(a: usize, b: usize) -> usize {
    a * (b / gcd(a, b))
}

pub fn lcm_vec(nums: &[usize]) -> usize {
    if nums.is_empty() {
        return 0;
    }

    let mut r: usize = nums[0];

    for &n in &nums[1..] {
        r = lcm(r, n);
    }

    r
}
