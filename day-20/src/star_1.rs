use std::collections::{HashMap, VecDeque};

use crate::base::*;

pub fn puzzle(input: String) -> usize {
    let mut data: HashMap<String, Cable> = parse(&input);

    // low and high pulses
    let mut total: (usize, usize) = (0, 0);
    let mut steps: usize = 0;

    while steps < 1000 {
        steps += 1;
        total.0 += 1;

        let broadcast: &Cable = data.get(&Module::Broadcaster.to_string()).unwrap();

        let mut queue: VecDeque<Cable> = VecDeque::new();
        queue.push_back(broadcast.clone());

        while let Some(cable) = queue.pop_front().as_mut() {
            for output in cable.get_outputs().iter() {
                let cable_pulse: &Pulse = cable.get_pulse();

                if cable_pulse.eq(&Pulse::Low) {
                    total.0 += 1;
                } else if cable_pulse.eq(&Pulse::High) {
                    total.1 += 1;
                }

                let next: &mut Cable = data.get_mut(output).unwrap();

                next.set_pulse(&cable_pulse, &cable.name);

                queue.push_back(next.clone());
            }
        }
    }

    total.0 * total.1
}
