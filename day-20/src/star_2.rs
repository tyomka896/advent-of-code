use std::collections::{HashMap, VecDeque};

use crate::base::*;
use crate::math::lcm_vec;

pub fn puzzle(input: String) -> usize {
    let mut data: HashMap<String, Cable> = parse(&input);

    let feed: &Cable = data
        .values()
        .find(|el| el.outputs.contains(&String::from("rx")))
        .unwrap();

    let inputs: Vec<String> = feed.inputs.iter().map(|el| el.name.to_owned()).collect();

    let mut highs: HashMap<String, usize> = HashMap::new();

    let mut pressed: usize = 0;

    loop {
        pressed += 1;

        let broadcast: &Cable = data.get(&Module::Broadcaster.to_string()).unwrap();

        let mut queue: VecDeque<Cable> = VecDeque::new();
        queue.push_back(broadcast.clone());

        while let Some(cable) = queue.pop_front().as_mut() {
            if inputs.contains(&cable.name) && cable.get_pulse().eq(&Pulse::High) {
                *highs.entry(cable.name.to_owned()).or_default() = pressed;
            }

            for output in cable.get_outputs().iter() {
                let cable_pulse: &Pulse = cable.get_pulse();

                let next: &mut Cable = data.get_mut(output).unwrap();

                next.set_pulse(&cable_pulse, &cable.name);

                queue.push_back(next.clone());
            }
        }

        if highs.len().eq(&inputs.len()) {
            break;
        }
    }

    let high_values: Vec<usize> = highs.values().map(|el| *el).collect::<Vec<usize>>();

    lcm_vec(&high_values)
}
