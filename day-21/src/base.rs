pub const NEIGHBORS: [(i32, i32); 4] = [(0, -1), (1, 0), (0, 1), (-1, 0)];

pub fn parse(input: &str) -> (Vec<Vec<char>>, (usize, usize)) {
    let mut start: (usize, usize) = (0, 0);

    let grid: Vec<Vec<char>> = input
        .lines()
        .enumerate()
        .fold(vec![], |mut acc, (l, line)| {
            let chars: Vec<char> = line.chars().collect();

            if let Some((p, _)) = chars.iter().enumerate().find(|&(_, el)| el.eq(&'S')) {
                start = (p, l)
            }

            acc.push(chars);

            acc
        });

    if start.eq(&(0, 0)) {
        panic!("Start position not found");
    }

    (grid, start)
}
