use super::base::*;

const TOTAL_STEPS: usize = 64;

pub fn puzzle(input: String) -> usize {
    let (mut data, start) = parse(&input);

    let mut current: Vec<(usize, usize)> = vec![start];

    let mut steps: usize = 0;

    while steps < TOTAL_STEPS {
        steps += 1;

        let mut newborn: Vec<(usize, usize)> = vec![];

        for l in 0..data.len() {
            for p in 0..data[l].len() {
                if data[l][p].eq(&'#') || data[l][p].eq(&'.') {
                    continue;
                }

                for (dx, dy) in NEIGHBORS {
                    let nx: i32 = p as i32 + dx;
                    let ny: i32 = l as i32 + dy;

                    if nx < 0
                        || ny < 0
                        || nx > data[l].len() as i32 - 1
                        || ny > data.len() as i32 - 1
                    {
                        continue;
                    }

                    let next: (usize, usize) = (nx as usize, ny as usize);

                    if data[next.1][next.0].eq(&'#') {
                        continue;
                    }

                    newborn.push(next);
                }
            }
        }

        for &(x, y) in &current {
            data[y][x] = '.'
        }

        for &(x, y) in &newborn {
            data[y][x] = 'O'
        }

        current.clear();
        current.append(&mut newborn);
    }

    data.iter().fold(0, |total, row| {
        total + row.iter().filter(|&el| el.eq(&'O')).count()
    })
}
