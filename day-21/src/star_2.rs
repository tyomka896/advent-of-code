use std::collections::{HashMap, VecDeque};

use super::base::*;

const TOTAL_STEPS: usize = 26501365;

fn steps_map(grid: &Vec<Vec<char>>, start: (usize, usize)) -> HashMap<(usize, usize), usize> {
    let mut visited: HashMap<(usize, usize), usize> = HashMap::new();

    let mut queue: VecDeque<((usize, usize), usize)> = VecDeque::new();
    queue.push_back((start, 0));

    while let Some((cell, steps)) = queue.pop_front() {
        if visited.contains_key(&cell) {
            continue;
        }

        visited.insert(cell, steps);

        for (dx, dy) in NEIGHBORS {
            let nx: i32 = cell.0 as i32 + dx;
            let ny: i32 = cell.1 as i32 + dy;

            if nx < 0 || ny < 0 || nx > grid[0].len() as i32 - 1 || ny > grid.len() as i32 - 1 {
                continue;
            }

            let next: (usize, usize) = (nx as usize, ny as usize);

            if grid[next.1][next.0].eq(&'#') {
                continue;
            }

            queue.push_back((next, steps + 1));
        }
    }

    visited
}

fn manhattan(from: (usize, usize), to: (usize, usize)) -> usize {
    from.0.abs_diff(to.0) + from.1.abs_diff(to.1)
}

pub fn puzzle(input: String) -> usize {
    // Explanation: https://github.com/villuna/aoc23/wiki/A-Geometric-solution-to-advent-of-code-2023,-day-21
    // For some input data, these calculations are valid and yield the correct result
    // and additional check of the Manhattan distance to the center not necessary.
    // For my input should add one to 'odd_corners' and another one to 'total'.

    let (data, start) = parse(&input);
    assert_eq!(data.len(), data[0].len());

    let steps_to_edge: usize = data.len() / 2;

    let distances: HashMap<(usize, usize), usize> = steps_map(&data, start);

    let even_full: usize = distances.iter().filter(|&(_, l)| *l % 2 == 0).count();

    let odd_full: usize = distances.iter().filter(|&(_, l)| *l % 2 == 1).count();

    let even_corners: usize = distances
        .iter()
        .filter(|&(cell, l)| {
            *l % 2 == 0 && *l > steps_to_edge && manhattan(start, *cell) > steps_to_edge
        })
        .count();

    let odd_corners: usize = distances
        .iter()
        .filter(|&(cell, l)| {
            *l % 2 == 1 && *l > steps_to_edge && manhattan(start, *cell) > steps_to_edge
        })
        .count();

    let steps: usize = TOTAL_STEPS / data.len();
    assert_eq!(steps, 202300);

    let total: usize = (steps + 1).pow(2) * odd_full + steps.pow(2) * even_full
        - (steps + 1) * odd_corners
        + steps * even_corners;

    total
}
