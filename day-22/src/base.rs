use std::collections::HashMap;

#[derive(Debug)]
pub struct Block {
    pub start: (u32, u32, u32),
    pub end: (u32, u32, u32),
}

impl Block {
    pub fn overlap(&self, other: &Self) -> bool {
        self.start.0.max(other.start.0) <= self.end.0.min(other.end.0)
            && self.start.1.max(other.start.1) <= self.end.1.min(other.end.1)
    }
}

impl From<&str> for Block {
    fn from(value: &str) -> Self {
        let [left, right] = value.split('~').collect::<Vec<&str>>().try_into().unwrap();

        let [x1, y1, z1] = left
            .split(',')
            .map(|el| el.parse::<u32>().unwrap())
            .collect::<Vec<u32>>()
            .try_into()
            .unwrap();

        let [x2, y2, z2] = right
            .split(',')
            .map(|el| el.parse::<u32>().unwrap())
            .collect::<Vec<u32>>()
            .try_into()
            .unwrap();

        Block {
            start: (x1.min(x2), y1.min(y2), z1.min(z2)),
            end: (x1.max(x2), y1.max(y2), z1.max(z2)),
        }
    }
}

pub fn settle_blocks(blocks: &mut Vec<Block>) {
    for l in 0..blocks.len() {
        let mut min_z: u32 = 1;

        for p in 0..l {
            if blocks[l].overlap(&blocks[p]) {
                min_z = min_z.max(blocks[p].end.2 + 1);
            }
        }

        blocks[l].end.2 -= min_z.abs_diff(blocks[l].start.2);
        blocks[l].start.2 = min_z;
    }

    blocks.sort_by_key(|el| el.start.2);
}

pub fn supports_info(
    blocks: &Vec<Block>,
) -> (HashMap<usize, Vec<usize>>, HashMap<usize, Vec<usize>>) {
    let mut above: HashMap<usize, Vec<usize>> = HashMap::new();
    let mut below: HashMap<usize, Vec<usize>> = HashMap::new();

    for l in 0..blocks.len() {
        above.insert(l, Vec::<usize>::new());
        below.insert(l, Vec::<usize>::new());
    }

    for l in 0..blocks.len() {
        for p in l..blocks.len() {
            if blocks[l].overlap(&blocks[p]) && blocks[l].end.2 == blocks[p].start.2 - 1 {
                above.entry(l).or_default().push(p);
                below.entry(p).or_default().push(l);
            }
        }
    }

    (above, below)
}

pub fn parse(input: &str) -> Vec<Block> {
    input.lines().fold(vec![], |mut acc, line| {
        acc.push(line.into());

        acc
    })
}
