use super::base::*;

pub fn puzzle(input: String) -> usize {
    let mut data: Vec<Block> = parse(&input);

    data.sort_by_key(|el| el.start.2);

    settle_blocks(&mut data);

    let (above, below) = supports_info(&data);

    let mut total: usize = 0;

    for l in 0..data.len() {
        let safely_removed: bool = above
            .get(&l)
            .unwrap()
            .iter()
            .all(|p| below.get(p).unwrap().len() >= 2);

        if safely_removed {
            total += 1;
        }
    }

    total
}
