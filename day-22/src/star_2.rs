use std::collections::{HashSet, VecDeque};

use super::base::*;

pub fn puzzle(input: String) -> usize {
    let mut data: Vec<Block> = parse(&input);

    data.sort_by_key(|el| el.start.2);

    settle_blocks(&mut data);

    let (above, below) = supports_info(&data);

    let mut total: usize = 0;

    for l in 0..data.len() {
        let mut queue: VecDeque<usize> = above
            .get(&l)
            .unwrap()
            .iter()
            .filter(|&p| below.get(p).unwrap().len() == 1)
            .cloned()
            .collect();

        let mut falling: HashSet<usize> = queue.iter().cloned().collect();
        falling.insert(l);

        while let Some(p) = queue.pop_front() {
            let difference: Vec<&usize> = above
                .get(&p)
                .unwrap()
                .iter()
                .filter(|&el| !falling.contains(el))
                .collect();

            for d in difference {
                if below.get(d).unwrap().iter().all(|el| falling.contains(el)) {
                    queue.push_back(*d);
                    falling.insert(*d);
                }
            }
        }

        total += falling.len() - 1;
    }

    total
}
