use std::collections::{BinaryHeap, HashMap, HashSet};

pub type Coord = (usize, usize);

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Node {
    pub coord: Coord,
    pub visited: HashSet<Coord>,
}

impl Node {
    pub fn from(coord: Coord) -> Self {
        Self {
            coord,
            visited: HashSet::from([coord]),
        }
    }

    pub fn next(&self, coord: Coord) -> Self {
        let mut new: Node = self.clone();

        new.coord = coord;
        new.visited.insert(coord);

        new
    }

    pub fn steps(&self) -> usize {
        self.visited.len() - 1
    }
}

impl Ord for Node {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.visited.len().cmp(&self.visited.len())
        // self.visited.len().cmp(&other.visited.len())
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(&other))
    }
}

pub struct Maze {
    edges: HashMap<Coord, Vec<Coord>>,
}

impl Maze {
    pub fn from(grid: Vec<Vec<char>>, directions: fn(char) -> Vec<(i32, i32)>) -> Self {
        let mut edges: HashMap<Coord, Vec<Coord>> = HashMap::new();

        for l in 0..grid.len() {
            for p in 0..grid[l].len() {
                if grid[l][p].eq(&'#') {
                    continue;
                }

                edges.entry((p, l)).or_insert(vec![]);

                for (dx, dy) in directions(grid[l][p]) {
                    let nx: i32 = p as i32 + dx;
                    let ny: i32 = l as i32 + dy;

                    if nx < 0
                        || ny < 0
                        || nx > grid[l].len() as i32 - 1
                        || ny > grid.len() as i32 - 1
                    {
                        continue;
                    }

                    let edge: Coord = (nx as usize, ny as usize);

                    if grid[edge.1][edge.0].eq(&'#') {
                        continue;
                    }

                    edges.entry((p, l)).and_modify(|el| el.push(edge));
                }
            }
        }

        // println!("edges.keys().len() -> {}", edges.keys().len());

        Self { edges }
    }

    fn _heuristic(left: Coord, right: Coord) -> usize {
        left.0.abs_diff(right.0) + left.1.abs_diff(right.1)
    }

    pub fn find_longest_path(&self, start: Coord, goal: Coord) -> usize {
        let mut longest: usize = 0;

        let mut queue: BinaryHeap<Node> = BinaryHeap::new();
        queue.push(Node::from(start));

        // let mut visited: HashSet<Coord> = HashSet::new();

        // let mut steps_map: HashMap<Coord, usize> = HashMap::new();
        // steps_map.insert(start, 0);

        // let mut connections: HashMap<Coord, Coord> = HashMap::new();

        while let Some(node) = queue.pop() {
            if node.coord.eq(&goal) {
                longest = longest.max(node.visited.len() - 1);

                continue;
            }

            for &next in self.edges.get(&node.coord).unwrap_or(&vec![]) {
                if node.visited.contains(&next) {
                    continue;
                }

                // if node.steps() + 1 < *steps_map.get(&next).unwrap_or(&0) {
                //     continue;
                // }

                let next_node: Node = node.next(next);

                queue.push(next_node);
                // steps_map.insert(next, next_node.steps());
                // connections.insert(next, next_node.steps());
            }

            // press();
        }

        longest
    }
}

pub fn parse(input: &str) -> Vec<Vec<char>> {
    input.lines().fold(vec![], |mut acc, line| {
        acc.push(line.chars().collect());

        acc
    })
}

pub fn _reconstruct_path(connections: &HashMap<Coord, Coord>, goal: &Coord) -> Vec<Coord> {
    let mut result: Vec<Coord> = vec![goal.clone()];
    let mut current: &Coord = goal;

    while let Some(node) = connections.get(&current) {
        result.push(node.clone());

        current = node;
    }

    result.reverse();

    result
}

pub fn _print_path(path: &Vec<Coord>, grid: &Vec<Vec<char>>) {
    println!("path.len() -> {}", path.len());

    for l in 0..grid.len() {
        let mut symbols: Vec<char> = vec![];

        for p in 0..grid[l].len() {
            if path.contains(&(p, l)) {
                symbols.push('O');
            } else if grid[l][p].eq(&'#') {
                symbols.push('#');
            } else {
                symbols.push(grid[l][p]);
            }
        }

        println!("{}", symbols.iter().collect::<String>());
    }
}
