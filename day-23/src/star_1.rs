use super::base::*;

fn directions(symbol: char) -> Vec<(i32, i32)> {
    match symbol {
        '.' => vec![(0, -1), (1, 0), (0, 1), (-1, 0)],
        '>' => vec![(1, 0)],
        'v' => vec![(0, 1)],
        _ => vec![],
    }
}

pub fn puzzle(input: String) -> usize {
    let data: Vec<Vec<char>> = parse(&input);

    let start: Coord = (1, 0);
    let goal: Coord = (data[0].len() - 2, data.len() - 1);

    let maze: Maze = Maze::from(data, directions);

    maze.find_longest_path(start, goal)
}
