use super::base::*;

fn directions(symbol: char) -> Vec<(i32, i32)> {
    match symbol {
        '.' | '>' | 'v' => vec![(0, -1), (1, 0), (0, 1), (-1, 0)],
        _ => vec![],
    }
}

pub fn puzzle(input: String) -> usize {
    // 5959 - low
    // Cannot solve this one by myself, it is far more complex for me

    let data: Vec<Vec<char>> = parse(&input);

    let start: Coord = (1, 0);
    let goal: Coord = (data[0].len() - 2, data.len() - 1);

    let maze: Maze = Maze::from(data, directions);

    let s = std::time::Instant::now();
    let result = maze.find_longest_path(start, goal);
    println!("s -> {}ms", s.elapsed().as_millis());

    result
}
