#[derive(Debug)]
struct Vector {
    x: f64,
    y: f64,
    z: f64,
}

impl From<Vec<f64>> for Vector {
    fn from(values: Vec<f64>) -> Self {
        if values.len() < 2 {
            return Self {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            };
        }

        Self {
            x: values[0],
            y: values[1],
            z: values[2],
        }
    }
}

#[derive(Debug)]
struct Equation {
    a: f64,
    b: f64,
}

impl Equation {
    pub fn from(pos: &Vector, vel: &Vector) -> Self {
        let x1: f64 = pos.x;
        let x2: f64 = pos.x + vel.x;

        let y1: f64 = pos.y;
        let y2: f64 = pos.y + vel.y;

        let slope: f64 = (y1 - y2) / (x1 - x2);

        Self {
            a: slope,
            b: slope * -x2 + y2,
        }
    }
}

#[derive(Debug)]
struct Hail {
    position: Vector,
    velocity: Vector,
    equation: Equation,
}

impl Hail {
    pub fn intersect(&self, other: &Self) -> Vector {
        let eq_left: &Equation = &self.equation;
        let eq_right: &Equation = &other.equation;

        let x: f64 = (eq_right.b - eq_left.b) / (eq_left.a - eq_right.a);
        let y: f64 = eq_left.a * x + eq_left.b;

        Vector { x, y, z: 0.0 }
    }
}

impl From<String> for Hail {
    fn from(value: String) -> Self {
        let [pos, vel] = value
            .split(" @ ")
            .collect::<Vec<&str>>()
            .try_into()
            .unwrap();

        let position: Vector = pos
            .split(", ")
            .map(|elem| elem.trim().parse::<f64>().unwrap_or(0.0))
            .collect::<Vec<f64>>()
            .into();

        let velocity: Vector = vel
            .split(", ")
            .map(|elem| elem.trim().parse::<f64>().unwrap_or(0.0))
            .collect::<Vec<f64>>()
            .into();

        let equation: Equation = Equation::from(&position, &velocity);

        Self {
            position,
            velocity,
            equation,
        }
    }
}

impl From<&str> for Hail {
    fn from(value: &str) -> Self {
        value.to_owned().into()
    }
}

fn is_inside(value: &Vector, range: &(usize, usize)) -> bool {
    value.x >= range.0 as f64
        && value.x <= range.1 as f64
        && value.y >= range.0 as f64
        && value.y <= range.1 as f64
}

fn is_in_future(hail: &Hail, vec: &Vector) -> bool {
    !(hail.velocity.y > 0.0 && vec.y < hail.position.y
        || hail.velocity.y < 0.0 && vec.y > hail.position.y)
}

pub fn puzzle(input: String) -> usize {
    // Test case: (7, 27)
    // Main case: (200000000000000, 400000000000000)

    let range: (usize, usize) = (200000000000000, 400000000000000);

    let mut total: usize = 0;

    let rows: Vec<&str> = input.lines().collect::<Vec<&str>>();

    for l in 0..rows.len() {
        for p in 1 + l..rows.len() {
            let left: Hail = rows[l].into();
            let right: Hail = rows[p].into();

            let intersect: Vector = left.intersect(&right);

            if !is_inside(&intersect, &range)
                || !is_in_future(&left, &intersect)
                || !is_in_future(&right, &intersect)
            {
                continue;
            }

            total += 1;
        }
    }

    total
}
