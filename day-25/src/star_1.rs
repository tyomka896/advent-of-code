use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;

use rustworkx_core::connectivity::stoer_wagner_min_cut;
use rustworkx_core::petgraph::graph::{NodeIndex, UnGraph};
use rustworkx_core::Result;

#[derive(Debug)]
struct Edge {
    pub name: String,
    pub left: String,
    pub right: String,
}

impl PartialEq for Edge {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Edge {
    pub fn from(left: &str, right: &str) -> Self {
        let mut names: Vec<&str> = vec![left, right];
        names.sort();

        Self {
            left: left.to_owned(),
            right: right.to_owned(),
            name: names.join("|"),
        }
    }
}

#[derive(Debug, Default)]
struct GraphData {
    pub vertices: Vec<String>,
    pub edges: Vec<Edge>,
}

impl GraphData {
    pub fn add_edge(&mut self, left: &str, right: &str) -> &mut Self {
        let edge: Edge = Edge::from(left, right);

        self.add_vertex(&left);
        self.add_vertex(&right);

        if !self.edges.contains(&edge) {
            self.edges.push(edge);
        }

        self
    }

    fn add_vertex(&mut self, value: &str) -> &mut Self {
        let value: String = value.to_owned();

        if !self.vertices.contains(&value) {
            self.vertices.push(value);
        }

        self
    }
}

fn create_graph(input: &str) -> GraphData {
    let mut graph_data: GraphData = GraphData::default();

    for line in input.lines() {
        let parts: Vec<&str> = line.split(": ").map(|elem| elem.trim()).collect();

        for name in parts[1].split(" ") {
            graph_data.add_edge(parts[0], name);
        }
    }

    graph_data
}

pub fn puzzle(input: String) -> usize {
    let graph_data: GraphData = create_graph(&input);
    println!(
        "<graph_data> -> {} vertices, {} edges.",
        graph_data.vertices.len(),
        graph_data.edges.len(),
        // graph_data.edges,
    );

    let mut un_graph: UnGraph<(), ()> = UnGraph::new_undirected();
    let mut edges_map: HashMap<&String, NodeIndex> =
        HashMap::with_capacity(graph_data.vertices.len());

    for vertex in &graph_data.vertices {
        edges_map.insert(vertex, un_graph.add_node(()));
    }

    let mut extend_edges: Vec<(NodeIndex, NodeIndex)> = vec![];

    for edge in &graph_data.edges {
        let left: &NodeIndex = edges_map.get(&edge.left).expect("NideIndex must exist");
        let right: &NodeIndex = edges_map.get(&edge.right).expect("NideIndex must exist");

        extend_edges.push((left.clone(), right.clone()));
    }

    un_graph.extend_with_edges(&extend_edges);

    let min_cut_res: Result<Option<(usize, Vec<_>)>> = stoer_wagner_min_cut(&un_graph, |_| Ok(1));
    let (_, partition) = min_cut_res.unwrap().unwrap();

    let left_part: usize = HashSet::<NodeIndex>::from_iter(partition).len();

    left_part * (graph_data.vertices.len() - left_part)
}
