/// It solves simple example.txt with Karger's algorithm implementation,
/// but can't solve the main data. It is too randomized and long to wait.
use rand::{self, Rng};

struct Edge {
    pub left: String,
    pub right: String,
    pub name: String,
}

impl std::fmt::Debug for Edge {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} : {:?} <-> {:?}", self.name, self.left, self.right,)
    }
}

impl PartialEq for Edge {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Edge {
    pub fn from(left: &str, right: &str) -> Self {
        let mut names: Vec<&str> = vec![left, right];
        names.sort();

        Self {
            left: left.to_owned(),
            right: right.to_owned(),
            name: names.join("|"),
        }
    }
}

#[derive(Debug, Default)]
struct Graph {
    pub vertices: Vec<String>,
    pub edges: Vec<Edge>,
}

impl Graph {
    pub fn add_edge(&mut self, left: &str, right: &str) -> &mut Self {
        let edge: Edge = Edge::from(left, right);

        self.add_vertex(&left);
        self.add_vertex(&right);

        if !self.edges.contains(&edge) {
            self.edges.push(edge);
        }

        self
    }

    fn add_vertex(&mut self, value: &str) -> &mut Self {
        let value: String = value.to_owned();

        if !self.vertices.contains(&value) {
            self.vertices.push(value);
        }

        self
    }
}

fn create_graph(input: &str) -> Graph {
    let mut graph: Graph = Graph::default();

    for line in input.lines() {
        let parts: Vec<&str> = line.split(": ").map(|elem| elem.trim()).collect();

        for name in parts[1].split(" ") {
            graph.add_edge(parts[0], name);
        }
    }

    graph
}

#[derive(Clone)]
struct Vertex {
    pub parent: String,
    pub name: String,
}

impl std::fmt::Debug for Vertex {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "p'{}' !-> n'{}'", self.parent, self.name)
    }
}

impl Vertex {
    pub fn from(value: &str) -> Self {
        Self {
            parent: value.to_owned(),
            name: value.to_owned(),
        }
    }
}

fn find_parent(vertices: &mut Vec<Vertex>, name: &str) -> String {
    let mut loop_count: i32 = 0;
    let mut parent: String = name.to_owned();

    while true && loop_count < 20 {
        loop_count += 1;

        if let Some(value) = vertices.iter().find(|elem| elem.name.eq(&parent)) {
            if value.parent.ne(&parent) {
                parent = value.parent.to_owned();
            } else {
                break;
            }
        }
    }

    parent
}

fn union_vertices(vertices: &mut Vec<Vertex>, left: &str, right: &str) {
    for vertex in vertices {
        if vertex.name.eq(&right) {
            vertex.parent = left.to_owned();
        }
    }
}

fn find_min_cut(graph: &Graph) -> Vec<Vertex> {
    println!("Starting to find min-cut. . .");

    let mut random = rand::thread_rng();
    let mut loop_count: usize = 0;

    while true && loop_count < 1000 {
        loop_count += 1;
        println!("<Attempt> -> #{loop_count}");

        let mut sub_vertices: Vec<Vertex> = Vec::with_capacity(graph.vertices.len());

        for name in graph.vertices.iter() {
            sub_vertices.push(Vertex::from(name));
        }

        let mut vertices_left: usize = sub_vertices.len();
        let mut search_ids: Vec<usize> = (0..graph.edges.len()).collect::<Vec<usize>>();

        while vertices_left > 2 && search_ids.len() > 0 {
            let l: usize = search_ids.remove(random.gen_range(0..search_ids.len()));

            let picked_edge: &Edge = &graph.edges[l];

            let left: String = find_parent(&mut sub_vertices, &picked_edge.left);
            let right: String = find_parent(&mut sub_vertices, &picked_edge.right);

            if left.eq(&right) {
                continue;
            }

            union_vertices(&mut sub_vertices, &left, &right);
            vertices_left -= 1;
        }

        let mut cut_edges: i32 = 0;
        for edge in &graph.edges {
            let subset1: String = find_parent(&mut sub_vertices, &edge.left);
            let subset2: String = find_parent(&mut sub_vertices, &edge.right);

            if subset1.ne(&subset2) {
                cut_edges += 1;
            }
        }
        println!("<cut_edges> -> {cut_edges}");

        if cut_edges == 3 {
            return sub_vertices;
        }
    }

    vec![]
}

fn find_group(result: &Vec<Vertex>, name: &str) -> usize {
    let mut unique = vec![name.to_owned()];
    let mut left_to_check = vec![name.to_owned()];

    while let Some(value) = left_to_check.pop() {
        for vertex in result {
            if vertex.name.ne(&value) && vertex.parent.ne(&value) {
                continue;
            }

            if vertex.name.eq(&value)
                && vertex.parent.ne(&value)
                && !unique.contains(&vertex.parent)
            {
                unique.push(vertex.parent.to_owned());
                left_to_check.push(vertex.parent.to_owned());
            }

            if vertex.name.ne(&value) && vertex.parent.eq(&value) && !unique.contains(&vertex.name)
            {
                unique.push(vertex.name.to_owned());
                left_to_check.push(vertex.name.to_owned());
            }
        }
    }

    unique.len()
}

pub fn puzzle(input: String) -> usize {
    let graph: Graph = create_graph(&input);
    println!(
        "graph -> {} vertices, {} edges.",
        graph.vertices.len(),
        graph.edges.len(),
        // graph.edges,
    );

    let result: Vec<Vertex> = find_min_cut(&graph);
    // println!("<result> {result:#?}");

    let doubles: Vec<String> = result
        .iter()
        .filter(|elem| elem.name.eq(&elem.parent))
        .map(|elem| elem.name.to_owned())
        .collect();

    let first: usize = find_group(&result, &doubles[0]);
    let second: usize = find_group(&result, &doubles[1]);

    println!("output: {first} - {second}");

    first * second
}
