use clap::Parser;
use std::path::PathBuf;

#[derive(Debug, Parser)]
pub struct Args {
    #[arg(long, value_delimiter = ',')]
    star: Vec<u8>,
    #[arg(long, value_delimiter = ',')]
    data: Vec<String>,
}

fn main() -> std::io::Result<()> {
    let args: Args = Args::parse();

    for (l, data_path) in args.data.iter().enumerate() {
        let input_data: String = std::fs::read_to_string(data_path)?;

        if args.data.len() > 1 {
            println!("Input '{}':", PathBuf::from(&data_path).file_name().unwrap().to_str().unwrap());
        }
        if args.star.contains(&1) {
            println!("⭐️   -> {:?}", day_09::star_1::puzzle(input_data.to_owned()));
        }
        if args.star.contains(&2) {
            println!("⭐️⭐️ -> {:?}", day_09::star_2::puzzle(input_data.to_owned()));
        }
        if l < args.data.len() - 1 {
            println!();
        }
    }

    Ok(())
}